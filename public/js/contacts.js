$("#form-contacts").validate({
    rules: {
        name: "required",
        email: {
            required: true,
            email: true
        },
        message: {
            required: true
        }
    },
    messages: {
        email: {
            required: "We need your email address to contact you",
            email: "Your email address must be in the format of name@domain.com"
        }
    },
    submitHandler: function (form) {
        var name = $('#contact-name').val()
        var email = $('#contact-email').val()
        var message = $('#contact-message').val()
        var data = JSON.stringify({
            name: name,
            email: email,
            message: message,
        });
        $.ajax('/api/contact-message', {
            type: 'POST',
            headers: {
                "Content-Type": "application/json"
            },
            dataType: 'json',
            data: data,
            success: function (data, status, xhr) {
                $("#ignismyModal").modal();
                $('#contact-name').val('')
                $('#contact-email').val('')
                $('#contact-message').val('')
            }
        })
    }
});