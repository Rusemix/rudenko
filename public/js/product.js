// $('#menu-product a').click(function () {
//     var typeProduct = $(this).attr('typeProduct');
//     if (typeProduct == 'all') {
//         $('#products .col-xl-4').fadeIn();
//     } else {
//         $('#products .col-xl-4').fadeOut();
//         $('.' + $(this).attr('typeProduct')).fadeIn();
//     }
//     return false;
// });
$(document).on('click', '.btn-filter-products', function(){
    var showStatus = $(this).attr('show');
    if (showStatus === 'true') {
        $(this).attr('show', 'false');
        $('#menu-product').css('display', 'none');
    }

    if(showStatus === 'false') {
        $(this).attr('show', 'true');
        $('#menu-product').css('display', 'block');
    }

});

$(document).on('click', '.close', function () {
    $("#products-block, #merch-block").modal('hide');
});


$(document).on('click', '#products .items a, #product-main-left-block .card-top a, #product-main-right-block .card-top a', function () {
    $('#products-block .modal-content').css('display', 'none');
    CopyToClipboard($(this).find('.code-product').text().trim());
    $('.buy-product-open').attr('disabled', 'disabled');
    $('.buy-product-open').addClass('disabled');
    $('.buy-product-open').parents('.block-buy').addClass('disabled');
    if (get_cookie('userIdIP')) {
        $('.block-steps').css('display', 'none')
        $('#step1').css('display', 'none')
        $('#step2').css('display', 'block')
    }

    var data = {};
    data.productId = $(this).attr('productid');
    $.ajax('/api/productInfo', {
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        data: JSON.stringify(data),  // data to submit
        success: function (data, status, xhr) {
            $('#products-block .modal-content').css('display', 'block');
            $('#img-product-list').html('');
            $('#products-block #step2 h2').text(data.name);
            $('#products-block #step2 .price-usd .priceNum').text(data.priceDiscount);
            $('#products-block #step2 .price-ua .priceNum').text(data.uah);
            $('#products-block #step2 .price-ru .priceNum').text(data.rub);
            $('#products-block #step2 proc').text(data.precent);
            $('#products-block #step2 .description').html(data.description);
            $('#products-block #step2 .promocode-text').text(data.promocode);
            $('#products-block #step2 .image-order #main-image-modal').attr('src', '/uploads/product/' + data.image);
            $('#products-block #step2 .image-order #main-image-modal-a').attr('href', '/uploads/product/' + data.image);
            $('#products-block #step2 .image-order #main-image-modal-a').attr('data-title', data.name);
            $('#products-block #step2 .buy-product-open').attr('url-product', data.url);
            $('#products-block #step2 .buy-product-open-amazon').attr('url-product', data.urlAmazon);
            $(data.attacment).each(function (k, v) {
                if (k > 2) {
                    $('#img-product-list').append('<a style="display: none" href="/uploads/product/' + v + '"' +
                        ' data-lightbox="image-order" data-title="Tactical Knife Yamato"><img src="/uploads/product/' + v + '"></a>');
                } else {
                    $('#img-product-list').append('<a href="/uploads/product/' + v + '"' +
                        ' data-lightbox="image-order" data-title="Tactical Knife Yamato"><img src="/uploads/product/' + v + '"></a>');
                }
            });

            if(!data.urlAmazon){
                $('.buy-product-open-amazon').css('display', 'none');
            }else{
                $('.buy-product-open-amazon').css('display', 'inline');
            }

            if (!data.promocode) {
                if(data.urlAmazon){
                    $('.block-buy-amazone').css('float', 'right');
                    $('.block-buy-amazone').css('margin-top', '5px');
                    $('.block-buy-amazone').css('width', 'auto');
                    $('.block-buy').css('float', 'left');
                }

                $('.copy-code-block').css('display', 'none');
                $('.block-buy button').removeClass('disabled');
                $('.block-buy').removeClass('disabled');
                $('.block-buy button').removeAttr('disabled');
            }else{
                $('.block-buy-amazone').css('float', 'left');
                $('.block-buy-amazone').css('margin-top', '15px');
                $('.block-buy-amazone').css('width', '100%');
                $('.block-buy').css('float', 'right');

                $('.copy-code-block').css('display', 'block');
                $('.block-buy button').addClass('disabled');
                $('.block-buy').addClass('disabled');
                $('.block-buy button').attr('disabled');
            }
        },
        error: function (jqXhr, textStatus, errorMessage) {
            $('p').append('Error' + errorMessage);
        }
    });
    $("#products-block").modal();
});

function nextStep(num) {
    $('#step' + num).css('display', 'none');
    $('#step' + (num + 1)).css('display', 'block');
    $('.block-steps div').removeClass('step-block-active');
    $('#step-block-' + (num + 1)).addClass('step-block-active');
}

$('.accept-conditions').click(function () {
    var agreeCheckbox = document.getElementById('agreeCheckbox');
    if (agreeCheckbox.checked) {
        set_cookie('userIdIP', true)
        $(this).addClass('next-step');
        var num = parseInt($(this).parents('.step').attr('num'));
        // console.log(num);
        nextStep(num);
    } else {
        $('.custom-checkbox').addClass('error');
    }
});

$('.copy-promocode').click(function () {
    $(this).removeClass('copy-promocode-animation');
    $('.buy-product-open').removeAttr('disabled');
    $('.buy-product-open').removeClass('disabled');
    $('.buy-product-open').parents('.block-buy').removeClass('disabled');
});

$(document).on('click', '.buy-product-open', function () {
    window.open($(this).attr('url-product'), '_blank');
});

$(document).on('click', '.buy-product-open-amazon', function () {
    window.open($(this).attr('url-product'), '_blank');
});


function get_cookie(cookie_name) {
    var results = document.cookie.match('(^|;) ?' + cookie_name + '=([^;]*)(;|$)');

    if (results)
        return ( unescape(results[2]) );
    else
        return null;
}

function set_cookie(name, value, exp_y, exp_m, exp_d, path, domain, secure) {
    var cookie_string = name + "=" + escape(value);

    if (exp_y) {
        var expires = new Date(exp_y, exp_m, exp_d);
        cookie_string += "; expires=" + expires.toGMTString();
    }

    if (path)
        cookie_string += "; path=" + escape(path);

    if (domain)
        cookie_string += "; domain=" + escape(domain);

    if (secure)
        cookie_string += "; secure";

    document.cookie = cookie_string;
}

function CopyToClipboard(text) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val(text).select();
    document.execCommand("copy");
    $temp.remove();
}


// var agreeCheckbox = document.getElementById('agreeCheckbox');
// if (agreeCheckbox.checked) {
//     $('.custom-checkbox').removeClass('error');
//     $.ajax({
//         url: '/getIp',
//         success: function (data) {
//             var json = JSON.parse(JSON.stringify(data, null, 2));
//         }
//     });
//     set_cookie('userIdIP', true)
//     sessionStorage.setItem('userIdIP', true);
//     var url = $(document).find("#urlRedirect").val();
//     var promoCode = $(document).find("#promoCode").text().trim();
//     $("#products-block").modal('hide');
//     CopyToClipboard(promoCode);
//     addDropshipping();
//     window.open(url, '_blank');
// } else {
//     $('.custom-checkbox').addClass('error');
// }

// return false


// $('#iagree .modal-content form').append('<input type="hidden" id="promoCode" value="' + $(this).find('.code-product').text().trim() + '">');
// $('#iagree .modal-content form').append('<input type="hidden" id="urlRedirect" value="' + $(this).attr('url-href').trim() + '">');
// $('#iagree .modal-content form').append('<input type="hidden" id="productsId" value="' + $(this).attr('productid').trim() + '">');
// CopyToClipboard($(this).find('.code-product').text().trim());
// if (!get_cookie('userIdIP')) {
//     if (!sessionStorage.getItem('userIdIP')) {
//         $("#products-block").modal();
//     } else {
//         addDropshipping()
//         var url = $(this).attr('url-href').trim();
//         window.open(url, '_blank');
//     }
// } else {
//     addDropshipping()
//     var url = $(this).attr('url-href').trim();
//     window.open(url, '_blank');
// }

//
// $(document).on('click', '#products .items a', function () {
//     $('#iagree .modal-content form').append('<input type="hidden" id="promoCode" value="' + $(this).find('.code-product').text().trim() + '">');
//     $('#iagree .modal-content form').append('<input type="hidden" id="urlRedirect" value="' + $(this).attr('url-href').trim() + '">');
//     $('#iagree .modal-content form').append('<input type="hidden" id="productsId" value="' + $(this).attr('productid').trim() + '">');
//     CopyToClipboard($(this).find('.code-product').text().trim());
//     if (!get_cookie('userIdIP')) {
//         if (!sessionStorage.getItem('userIdIP')) {
//             $("#products-block").modal();
//         } else {
//             addDropshipping()
//             var url = $(this).attr('url-href').trim();
//             window.open(url, '_blank');
//         }
//     } else {
//         addDropshipping()
//         var url = $(this).attr('url-href').trim();
//         window.open(url, '_blank');
//     }
// });
// $(document).on('click', '.buy-procut-drop', function () {
//     // var agreeCheckbox = document.getElementById('agreeCheckbox');
//     // if (agreeCheckbox.checked) {
//     //     $('.custom-checkbox').removeClass('error');
//     //     $.ajax({
//     //         url: '/getIp',
//     //         success: function (data) {
//     //             var json = JSON.parse(JSON.stringify(data, null, 2));
//     //         }
//     //     });
//     //     set_cookie('userIdIP', true)
//     //     sessionStorage.setItem('userIdIP', true);
//     //     var url = $(document).find("#urlRedirect").val();
//     //     var promoCode = $(document).find("#promoCode").text().trim();
//     //     $("#products-block").modal('hide');
//     //     CopyToClipboard(promoCode);
//     //     addDropshipping();
//     //     window.open(url, '_blank');
//     // } else {
//     //     $('.custom-checkbox').addClass('error');
//     // }
//
//     return false
// });

function addDropshipping() {
    $.ajax({
        url: '/api/dropshippingAdd',
        data: {productId: 11},
        type: "POST",
        success: function (data) {
            var json = JSON.parse(JSON.stringify(data, null, 2));

            set_cookie('userIdIP', json.geoplugin_request)
            sessionStorage.setItem('userIdIP', json.geoplugin_request);
        }
    });
}

