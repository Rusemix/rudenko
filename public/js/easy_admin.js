// $('.field-collection-item-action').click(function () {
//     var img = $(this).parents('.field-attachment_merch').find('.easyadmin-thumbnail img').attr('src');
//     console.log(img);
//     // $.ajax('/api/removeImgAttachment', {
//     //     type: 'POST',
//     //     data: {imgSrc: img, type: 'merch'},  // data to submit
//     //     success: function (data, status, xhr) {
//     //     },
//     //     error: function (jqXhr, textStatus, errorMessage) {
//     //     }
//     // })
// });
$(document).ready(function () {
    var data = {}
    $.ajax('/api/count-new-data', {
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        data: JSON.stringify(data),
        success: function (data, status, xhr) {
            var order = 0;
            var preorder = 0;
            var serviceSum = 0;
            var service = data.countNewData.service;
            if(data.countNewData.order){
                order = data.countNewData.order;
            }
            if(data.countNewData.preorder){
                preorder = data.countNewData.preorder;
            }
            if(service.sum){
                serviceSum = service.sum;
            }
            $('.order-menu  span').html($('.order-menu span').text() + ' - <span style="color:red">(' + order + ')</span>');
            $('.preorder-menu  span').html($('.preorder-menu span').text() + ' - <span style="color:red">(' + preorder + ')</span>');
            $('.services-menu span').html($('.services-menu span').text() + ' - <span style="color:red">(' + serviceSum + ')</span>');
            for (var i = 1; i <= 6; i++) {
                var count = 0;
                if (service[i]) {
                    count = service[i];
                }
                $('.services-list-menu-' + i + ' span').html($('.services-list-menu-' + i + ' span').text() + ' - <span style="color:red">(' + count + ')</span>')
            }
        }
    });
})