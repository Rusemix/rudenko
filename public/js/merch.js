$(document).ready(function () {
    $(document).on('click', '.items', function () {
        var productId = $(this).find('a').attr('productid');
        var urlRequest = '/api/merchInfo';
        var data = {};
        data.productId = productId;
        data.locale = $('html').attr('lang');
        $.ajax(urlRequest, {
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            data: JSON.stringify(data),
            success: function (data, status, xhr) {
                $('.tmpl-merch').remove();
                $('#merchTmpl').tmpl(data).appendTo('#merch-block');
                $("#merch-block").modal();
                if(data.size != 1)
                {
                    $('.size-order').css('display', 'none');
                    $('#sizeProduct').val('');
                }
            }
        });
    })
});
$(document).on('click', '.size-order span', function () {
    $('.size-order span').removeClass('active-order');
    $(this).addClass('active-order');
    $('#sizeProduct').val($(this).text());
});
$(document).on('change', '#country', function () {
    var url = $(this).find(':selected').attr('url-items');
    if (url) {
        $('#control-block-first-step .next-step-merch').css('display', 'none');
        $('#control-block-first-step .open-product-magazine').css('display', 'block');
    } else {
        $('#control-block-first-step .open-product-magazine').css('display', 'none');
        $('#control-block-first-step .next-step-merch').css('display', 'block');
    }
});

$(document).on('click', '.open-product-magazine', function () {
    var url = $('#country').find(':selected').attr('url-items');
    window.open(url, '_blank');
});

$(document).on('click', '.close ', function () {
    $("#merch-block").modal('hide');
});

$(document).on('click', '#control-block-first-step .next-step-merch', function () {
    $('#step2').css('display', 'block');
    $('#step1').css('display', 'none');
    var valCountry = $('#country').find(':selected').val();
    if (valCountry == 'UA') {
        $('.change-ukraine').css('display', 'block');
        $('.buy-product').css('display', 'block');
        $('.preorder-product').css('display', 'none');
        $('.change-another').css('display', 'none');
    }
    if (valCountry == 'variant') {
        $('.change-ukraine').css('display', 'none');
        $('.buy-product').css('display', 'none');
        $('.preorder-product').css('display', 'block');
        $('.change-another').css('display', 'block');
    }
});

$(document).on('click', '.buy-product', function () {
    $("#order-merch").validate({
        rules: {
            size: "required",
            surname_name: "required",
            phone: "required",
            city: "required",
            nova_poshta: "required",
            email: {
                required: true,
                email: true
            }
        },
        messages: {
            email: {
                required: "We need your email address to contact you",
                email: "Your email address must be in the format of name@domain.com"
            }
        },
        submitHandler: function(form) {
            var urlRequest = '/api/save-order';
            var country = $('#country').find(':selected').text();
            var data = {};
            data.data = $('#order-merch').serialize();
            data.country = country;
            $.ajax(urlRequest, {
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                data: JSON.stringify(data),
                success: function (data, status, xhr) {
                    if (data.status == true) {
                        $("#merch-block").modal('hide');
                        $("#ignismyModal").modal();
                        $(".cupon-pop span").text(data.orderId);
                    }
                }
            });

        }
    });
});


$(document).on('click', '.preorder-product', function () {

    $("#order-merch").validate({
        rules: {
            surname_name: "required",
            phone: "required",
            city: "required",
            country: "required",
            address: "required",
            email: {
                required: true,
                email: true
            }
        },
        messages: {
            email: {
                required: "We need your email address to contact you",
                email: "Your email address must be in the format of name@domain.com"
            }
        },
        submitHandler: function(form) {
            var urlRequest = '/api/save-preorder';
            var data = {};
            data.data = $('#order-merch').serialize();
            $.ajax(urlRequest, {
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                data: JSON.stringify(data),
                success: function (data, status, xhr) {
                    $("#merch-block").modal('hide');
                    $("#ignismyModal").modal();
                    $(".cupon-pop span").text(data.orderId);
                }
            });
        }
    });
});

$(document).on('click', '.prev-step-merch', function () {
    var num = parseInt($(this).parents('.step').attr('num'));
    prevStep(num)
    return false
});

function prevStep(num) {
    $('#step' + num).css('display', 'none');
    $('#step' + (num - 1)).css('display', 'block');
    $('.block-steps div').removeClass('step-block-active');
    $('#step-block-' + (num - 1)).addClass('step-block-active');
}