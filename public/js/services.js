$(function () {
    $("#datepicker").datetimepicker({
        inline: true,
        sideBySide: true,
        daysOfWeekDisabled: [1, 2, 3, 4, 5],
        enabledHours: [7, 8, 9, 10, 11, 19, 20, 21, 22, 23],
        format: 'YYYY-MM-DD HH:mm A',
    });

    $('.uslug-menu').click(function () {
        $('#payment-form').css('display', 'none');
        $('.form-control').val('')
        $('#paypal-button').html('');
        if ($(this).attr('type-pay') == 'true') {
            $('.send-services').css('display', 'none');
            $('.pay-services').css('display', 'block');
            $('.pay-services .amount-pay').text($(this).attr('amount'));
        } else {
            $('.send-services').css('display', 'block');
            $('.pay-services').css('display', 'none');
        }
        $('#nameUslug').text($(this).text());
        $('#type-services').val($(this).attr('type-id'));
    })

    $("#services-form").validate({
        rules: {
            name: "required",
            email: {
                required: true,
                email: true
            }
        },
        messages: {
            email: {
                required: "We need your email address to contact you",
                email: "Your email address must be in the format of name@domain.com"
            }
        },
        submitHandler: function (form) {
            var name = $('#services-form #name').val();
            var email = $('#services-form #email').val();
            var type = $('#services-form #type-services').val();
            var datepicker = $('#services-form #datepicker').val();
            var message = $('#services-form textarea').val();
            var codeAuth = $('#services-form #code_auth').val();
            var data = JSON.stringify({
                name: name,
                email: email,
                type: parseInt(type),
                datetime: datepicker,
                message: message,
                codeAuth: codeAuth
            });
            $.ajax('/api/add/services', {
                type: 'POST',
                headers: {
                    "Content-Type": "application/json"
                },
                dataType: 'json',
                data: data,
                success: function (data, status, xhr) {
                    if (data.pay === true) {

                        var stripe = Stripe("pk_live_51HMmbmGllLgrgIs5e6wDfLH0zEZ4VnWV6cxKArqhZf7TGTLXnCDr0V505VAR3mDeoskRmLXgvQDXxnnpmFG0rZPm00uLr61MIp");
                        var purchase = {
                            amount: data.amount,
                            datetime: data.datetime,
                            email: data.email,
                            serviceId: data.id,
                            name: data.name,
                            type: data.type
                        };
                        // Disable the button until we have Stripe set up on the page
                        document.querySelector("button").disabled = true;
                        fetch("/api/create-payment", {
                            method: "POST",
                            headers: {
                                "Content-Type": "application/json"
                            },
                            body: JSON.stringify(purchase)
                        })
                            .then(function (result) {
                                return result.json();
                            })
                            .then(function (data) {
                                var elements = stripe.elements();
                                var style = {
                                    base: {
                                        color: "#32325d",
                                        fontFamily: 'Arial, sans-serif',
                                        fontSmoothing: "antialiased",
                                        fontSize: "16px",
                                        "::placeholder": {
                                            color: "#32325d"
                                        }
                                    },
                                    invalid: {
                                        fontFamily: 'Arial, sans-serif',
                                        color: "#fa755a",
                                        iconColor: "#fa755a"
                                    }
                                };
                                var card = elements.create("card", {style: style});
                                // Stripe injects an iframe into the DOM
                                card.mount("#card-element");
                                card.on("change", function (event) {
                                    // Disable the Pay button if there are no card details in the Element
                                    document.querySelector("#payment-form button").disabled = event.empty;
                                    document.querySelector("#payment-form #card-error").textContent = event.error ? event.error.message : "";
                                });
                                var form = document.getElementById("payment-form");
                                form.addEventListener("submit", function (event) {
                                    event.preventDefault();
                                    // Complete payment when the submit button is clicked
                                    payWithCard(stripe, card, data.clientSecret);
                                });
                            });

                        $('#payment-form').css('display', 'block');
                        $('.pay-services').css('display', 'none');
                    } else {
                        $("#uslug").modal('hide');
                        $("#ignismyModal").modal();
                        $(".cupon-pop span").text(data.id);
                        $(".name-clients").text(data.name);
                        $(".date-clients").text(data.datetime);
                        $(".theme-clients").text(data.type);
                    }
                }
            });
        }
    });

    function getDates(array) {
        var dateArray = new Array();
        $(array).each(function (i, v) {
            dateArray.push(v)
        });
        return dateArray;
    }

    var payWithCard = function (stripe, card, clientSecret) {
        loading(true);
        stripe
            .confirmCardPayment(clientSecret, {
                payment_method: {
                    card: card
                }
            })
            .then(function (result) {
                if (result.error) {
                    // Show error to your customer
                    showError(result.error.message);
                } else {
                    // The payment succeeded!
                    orderComplete(result.paymentIntent.id);
                    $("#uslug").modal('hide');
                    $("#ignismyModal").modal();
                    $('.cupon-pop').text($('.result-message').text());
                    $('.cupon-pop').css('display', 'block');
                }
            });
    };

    /* ------- UI helpers ------- */
    // Shows a success message when the payment is complete
    var orderComplete = function (paymentIntentId) {
        loading(false);
        document.querySelector("#payment-form .result-message").classList.remove("hidden");
        document.querySelector("#payment-form button").disabled = true;
    };
    // Show the customer the error from Stripe if their card fails to charge
    var showError = function (errorMsgText) {
        loading(false);
        var errorMsg = document.querySelector("#card-error");
        errorMsg.textContent = errorMsgText;
        setTimeout(function () {
            errorMsg.textContent = "";
        }, 4000);
    };
    // Show a spinner on payment submission
    var loading = function (isLoading) {
        if (isLoading) {
            // Disable the button and show a spinner
            document.querySelector("#payment-form button").disabled = true;
            document.querySelector("#payment-form #spinner").classList.remove("hidden");
            document.querySelector("#payment-form #button-text").classList.add("hidden");
        } else {
            document.querySelector("#payment-form button").disabled = false;
            document.querySelector("#payment-form #spinner").classList.add("hidden");
            document.querySelector("#payment-form #button-text").classList.remove("hidden");
        }
    };
});