<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nameUa;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nameRu;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $secondName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $secondNameUa;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $secondNameRu;

    /**
     * @ORM\Column(type="boolean", options={"default": true})
     */
    private $status = true;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TypeCategory", inversedBy="categories")
     * @ORM\JoinColumn(nullable=false)
     */
    private $typeCategory;

//    /**
//     * @ORM\OneToMany(targetEntity="App\Entity\Product", mappedBy="category")
//     */
//    private $products;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"default": 0})
     */
    private $weight;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $classCss;

    public function __construct()
    {
        $this->products = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSecondName(): ?string
    {
        return $this->secondName;
    }

    public function setSecondName(?string $secondName): self
    {
        $this->secondName = $secondName;

        return $this;
    }

    public function getNameUa()
    {
        return $this->nameUa;
    }

    public function setNameUa($nameUa): void
    {
        $this->nameUa = $nameUa;
    }


    public function getNameRu()
    {
        return $this->nameRu;
    }

    public function setNameRu($nameRu): void
    {
        $this->nameRu = $nameRu;
    }


    public function getSecondNameUa()
    {
        return $this->secondNameUa;
    }


    public function setSecondNameUa($secondNameUa): void
    {
        $this->secondNameUa = $secondNameUa;
    }


    public function getSecondNameRu()
    {
        return $this->secondNameRu;
    }

    public function setSecondNameRu($secondNameRu): void
    {
        $this->secondNameRu = $secondNameRu;
    }

    public function getTypeCategory(): ?TypeCategory
    {
        return $this->typeCategory;
    }

    public function setTypeCategory(?TypeCategory $typeCategory): self
    {
        $this->typeCategory = $typeCategory;

        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

//    public function getProducts(): Collection
//    {
//        return $this->products;
//    }
//
//    public function addProduct(Product $product): self
//    {
//        if (!$this->products->contains($product)) {
//            $this->products[] = $product;
//            $product->setCategory($this);
//        }
//
//        return $this;
//    }
//
//    public function removeProduct(Product $product): self
//    {
//        if ($this->products->contains($product)) {
//            $this->products->removeElement($product);
//            // set the owning side to null (unless already changed)
//            if ($product->getCategory() === $this) {
//                $product->setCategory(null);
//            }
//        }
//
//        return $this;
//    }

    public function getWeight()
    {
        return $this->weight;
    }

    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    public function getClassCss(): ?string
    {
        return $this->classCss;
    }

    public function setClassCss(?string $classCss)
    {
        $this->classCss = $classCss;
    }

    public function __toString()
    {
        return $this->typeCategory . ' - ' . $this->nameRu;
    }
}
