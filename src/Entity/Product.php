<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 * @Vich\Uploadable()
 */
class Product
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titleUa;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titleRu;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $buyAmazon;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @ORM\OneToMany(targetEntity="Attachment", mappedBy="product", cascade={"persist"})
     */
    private $attachment;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $price;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $newPrice;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $precent;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"default": 0})
     */
    private $count;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $uah;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $rub;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Category")
     */
    private $category;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $promocode;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @Vich\UploadableField(mapping="product_image", fileNameProperty="image")
     */
    private $imageFile;

    /**
     * @ORM\Column(type="boolean", options={"default": true})
     */
    private $status;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"default": 0})
     */
    private $mainView;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"default": 0})
     */
    private $countXs;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"default": 0})
     */
    private $countS;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"default": 0})
     */
    private $countM;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"default": 0})
     */
    private $countL;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"default": 0})
     */
    private $countXl;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"default": 0})
     */
    private $countXxl;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default": false})
     */
    private $optionSize;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $descriptionUa;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $descriptionRu;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
        $this->category = new ArrayCollection();
        $this->attachment = new ArrayCollection();
    }

    public function getBuyAmazon()
    {
        return $this->buyAmazon;
    }

    public function setBuyAmazon($buyAmazon)
    {
        $this->buyAmazon = $buyAmazon;
    }

    public function getNewPrice()
    {
        return $this->newPrice;
    }

    public function setNewPrice($newPrice)
    {
        $this->newPrice = $newPrice;
    }

    public function getMainView()
    {
        return $this->mainView;
    }

    public function setMainView(?int $mainView)
    {
        $this->mainView = $mainView;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        $locale = $GLOBALS['request']->getLocale();
        if ($locale == 'ua') {
            $title = $this->titleUa;
        } elseif ($locale == 'ru') {
            $title = $this->titleRu;
        } elseif ($locale == 'en') {
            $title = $this->title;
        }
        if (empty($title)) {
            $title = $this->title;
        }
        return $title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getTitleUa()
    {
        return $this->titleUa;
    }

    public function setTitleUa($titleUa)
    {
        $this->titleUa = $titleUa;
    }

    public function getTitleRu()
    {
        return $this->titleRu;
    }

    public function setTitleRu($titleRu)
    {
        $this->titleRu = $titleRu;
    }


    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function setImageFile($imageFile)
    {
        $this->imageFile = $imageFile;

        if ($imageFile) {
            $this->updatedAt = new \DateTime();
        }
    }


    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(?float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getPrecent(): ?float
    {
        return $this->precent;
    }

    public function setPrecent(?float $precent): self
    {
        $this->precent = $precent;

        return $this;
    }

    public function getUah(): ?float
    {
        return $this->uah;
    }

    public function setUah(?float $uah): self
    {
        $this->uah = $uah;

        return $this;
    }

    public function getRub(): ?float
    {
        return $this->rub;
    }

    public function setRub(?float $rub): self
    {
        $this->rub = $rub;

        return $this;
    }

    public function getCategory(): Collection
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        if (!$this->category->contains($category)) {
            $this->category[] = $category;
        }

        return $this;
    }

    public function getPromocode(): ?string
    {
        return $this->promocode;
    }

    public function setPromocode(?string $promocode): self
    {
        $this->promocode = $promocode;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getCount(): ?int
    {
        return $this->count;
    }

    public function setCount(?int $count)
    {
        $this->count = $count;
    }

    public function removeCategory(Category $category): self
    {
        if ($this->category->contains($category)) {
            $this->category->removeElement($category);
        }

        return $this;
    }

    public function __toString()
    {
        return $this->title;
    }

    public function getCountXs(): ?int
    {
        return $this->countXs;
    }

    public function setCountXs(?int $countXs): self
    {
        $this->countXs = $countXs;

        return $this;
    }

    public function getCountS(): ?int
    {
        return $this->countS;
    }

    public function setCountS(?int $countS): self
    {
        $this->countS = $countS;

        return $this;
    }

    public function getCountM()
    {
        return $this->countM;
    }

    public function setCountM($countM)
    {
        $this->countM = $countM;
    }

    public function getCountL()
    {
        return $this->countL;
    }

    public function setCountL($countL)
    {
        $this->countL = $countL;
    }

    public function getCountXl()
    {
        return $this->countXl;
    }

    public function setCountXl($countXl)
    {
        $this->countXl = $countXl;
    }

    public function getCountXxl()
    {
        return $this->countXxl;
    }

    public function setCountXxl($countXxl)
    {
        $this->countXxl = $countXxl;
    }

    public function getOptionSize(): ?bool
    {
        return $this->optionSize;
    }

    public function setOptionSize(?bool $optionSize): self
    {
        $this->optionSize = $optionSize;

        return $this;
    }

    public function getAttachment(): Collection
    {
        return $this->attachment;
    }

    public function addAttachment(Attachment $attachment): self
    {
        if (!$this->attachment->contains($attachment)) {
            $this->attachment[] = $attachment;
            $attachment->setProduct($this);
        }

        return $this;
    }

    public function removeAttachment(Attachment $attachment): self
    {
        if ($this->attachment->contains($attachment)) {
            $this->attachment->removeElement($attachment);
            // set the owning side to null (unless already changed)
            if ($attachment->getProduct() === $this) {
                $attachment->setProduct(null);
            }
        }

        return $this;
    }

    public function addCategory(Category $category): self
    {
        if (!$this->category->contains($category)) {
            $this->category[] = $category;
        }

        return $this;
    }

    public function getDescription(): ?string
    {
        $locale = $GLOBALS['request']->getLocale();
        if ($locale == 'ua') {
            $description = $this->descriptionUa;
        } elseif ($locale == 'ru') {
            $description = $this->descriptionRu;
        } elseif ($locale == 'en') {
            $description = $this->description;
        }
        if (empty($description)) {
            $description = $this->description;
        }
        return $description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDescriptionUa(): ?string
    {
        return $this->descriptionUa;
    }

    public function setDescriptionUa(?string $descriptionUa): self
    {
        $this->descriptionUa = $descriptionUa;

        return $this;
    }

    public function getDescriptionRu(): ?string
    {
        return $this->descriptionRu;
    }

    public function setDescriptionRu(?string $descriptionRu): self
    {
        $this->descriptionRu = $descriptionRu;

        return $this;
    }

    public function getDiscountPrice()
    {
        $price = 0;
        if ($this->price) {
            if ($this->newPrice) {
                $price = $this->newPrice;
            } else {
                $price = round($this->price - ($this->precent * ($this->price / 100)), 2);
            }
        }
        return $price;
    }

    public function getNewPrecent()
    {
        $precent = 0;
        if ($this->price) {
            if ($this->newPrice) {
                $precent = round(100 - ($this->newPrice / ($this->price / 100)));
            } else {
                $precent = $this->precent;
            }
        }
        return $precent;
    }
}
