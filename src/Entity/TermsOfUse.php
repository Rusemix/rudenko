<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TermsOfUseRepository")
 */
class TermsOfUse
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $text;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $textUa;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $textRu;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(?string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getTextUa(): ?string
    {
        return $this->textUa;
    }

    public function setTextUa(?string $textUa): self
    {
        $this->textUa = $textUa;

        return $this;
    }

    public function getTextRu(): ?string
    {
        return $this->textRu;
    }

    public function setTextRu(?string $textRu): self
    {
        $this->textRu = $textRu;

        return $this;
    }
}
