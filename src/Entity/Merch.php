<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MerchRepository")
 * @Vich\Uploadable()
 */
class Merch
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $titleUa;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $titleRu;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $newPrice;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $usd;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $uah;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $rub;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $precent;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="merch_attachments", fileNameProperty="image")
     */
    private $imageFile;

    /**
     * @ORM\OneToMany(targetEntity="AttachmentMerch", mappedBy="product")
     */
    private $attachment;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $promocode;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Category")
     */
    private $category;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default": 1})
     */
    private $status = true;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"default": 0})
     */
    private $countXs;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"default": 0})
     */
    private $countS;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"default": 0})
     */
    private $countM;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"default": 0})
     */
    private $countL;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"default": 0})
     */
    private $countXl;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"default": 0})
     */
    private $countXxl;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default": false})
     */
    private $optionSize;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $descriptionUa;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $descriptionRu;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $urlUa;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $urlRu;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
        $this->category = new ArrayCollection();
        $this->attachment = new ArrayCollection();

    }

    public function getName($locale = null): ?string
    {
        if ($locale == 'ua') {
            $title = $this->titleUa;
        } elseif ($locale == 'ru') {
            $title = $this->titleRu;
        } elseif ($locale == 'en') {
            $title = $this->title;
        }
        if (empty($title)) {
            $title = $this->title;
        }
        return $title;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {

        $this->title = $title;

        return $this;
    }

    public function getTitleUa(): ?string
    {
        return $this->titleUa;
    }

    public function setTitleUa(?string $titleUa): self
    {
        $this->titleUa = $titleUa;

        return $this;
    }

    public function getTitleRu(): ?string
    {
        return $this->titleRu;
    }

    public function setTitleRu(?string $titleRu): self
    {
        $this->titleRu = $titleRu;

        return $this;
    }

    public function getUsd(): ?float
    {
        return $this->usd;
    }

    public function setUsd(?float $usd): self
    {
        $this->usd = $usd;

        return $this;
    }

    public function getUah(): ?float
    {
        return $this->uah;
    }

    public function setUah(?float $uah): self
    {
        $this->uah = $uah;

        return $this;
    }

    public function getRub(): ?float
    {
        return $this->rub;
    }

    public function setRub(?float $rub): self
    {
        $this->rub = $rub;

        return $this;
    }

    public function getPrecent(): ?float
    {
        return $this->precent;
    }

    public function setPrecent(?float $precent): self
    {
        $this->precent = $precent;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return Collection|Category[]
     */
    public function getCategory(): Collection
    {
        return $this->category;
    }

    public function addCategory(Category $category): self
    {
        if (!$this->category->contains($category)) {
            $this->category[] = $category;
        }

        return $this;
    }

    public function removeCategory(Category $category): self
    {
        if ($this->category->contains($category)) {
            $this->category->removeElement($category);
        }

        return $this;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(?bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param mixed $imageFile
     */
    public function setImageFile($imageFile)
    {
        $this->imageFile = $imageFile;

        if ($imageFile) {
            $this->updatedAt = new \DateTime();
        }
    }

    /**
     * @return mixed
     */
    public function getPromocode()
    {
        return $this->promocode;
    }

    /**
     * @param mixed $promocode
     */
    public function setPromocode($promocode)
    {
        $this->promocode = $promocode;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return mixed
     */
    public function getCountXs()
    {
        return $this->countXs;
    }

    /**
     * @param mixed $countXs
     */
    public function setCountXs($countXs)
    {
        $this->countXs = $countXs;
    }

    /**
     * @return mixed
     */
    public function getCountS()
    {
        return $this->countS;
    }

    /**
     * @param mixed $countS
     */
    public function setCountS($countS)
    {
        $this->countS = $countS;
    }

    /**
     * @return mixed
     */
    public function getCountM()
    {
        return $this->countM;
    }

    /**
     * @param mixed $countM
     */
    public function setCountM($countM)
    {
        $this->countM = $countM;
    }

    /**
     * @return mixed
     */
    public function getCountL()
    {
        return $this->countL;
    }

    /**
     * @param mixed $countL
     */
    public function setCountL($countL)
    {
        $this->countL = $countL;
    }

    /**
     * @return mixed
     */
    public function getCountXl()
    {
        return $this->countXl;
    }

    /**
     * @param mixed $countXl
     */
    public function setCountXl($countXl)
    {
        $this->countXl = $countXl;
    }

    /**
     * @return mixed
     */
    public function getCountXxl()
    {
        return $this->countXxl;
    }

    /**
     * @param mixed $countXxl
     */
    public function setCountXxl($countXxl)
    {
        $this->countXxl = $countXxl;
    }

    /**
     * @return mixed
     */
    public function getOptionSize()
    {
        return $this->optionSize;
    }

    /**
     * @param mixed $optionSize
     */
    public function setOptionSize($optionSize)
    {
        $this->optionSize = $optionSize;
    }

    /**
     * @return Collection|Attachment[]
     */
    public function getAttachment(): Collection
    {
        return $this->attachment;
    }

    public function addAttachment(AttachmentMerch $attachment): self
    {
        if (!$this->attachment->contains($attachment)) {
            $this->attachment[] = $attachment;
            $attachment->setProduct($this);
        }

        return $this;
    }

    public function removeAttachment(AttachmentMerch $attachment): self
    {
        if ($this->attachment->contains($attachment)) {
            $this->attachment->removeElement($attachment);
            // set the owning side to null (unless already changed)
            if ($attachment->getProduct() === $this) {
                $attachment->setProduct(null);
            }
        }

        return $this;
    }

    public function getDescription($locale = null): ?string
    {
        if ($locale == 'ua') {
            $description = $this->descriptionUa;
        } elseif ($locale == 'ru') {
            $description = $this->descriptionRu;
        } elseif ($locale == 'en') {
            $description = $this->description;
        }
        if (empty($description)) {
            $description = $this->description;
        }
        return $description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDescriptionUa(): ?string
    {
        return $this->descriptionUa;
    }

    public function setDescriptionUa(?string $descriptionUa): self
    {
        $this->descriptionUa = $descriptionUa;

        return $this;
    }

    public function getDescriptionRu(): ?string
    {
        return $this->descriptionRu;
    }

    public function setDescriptionRu(?string $descriptionRu): self
    {
        $this->descriptionRu = $descriptionRu;

        return $this;
    }

    public function getUrlUa(): ?string
    {
        return $this->urlUa;
    }

    public function setUrlUa(?string $urlUa): self
    {
        $this->urlUa = $urlUa;

        return $this;
    }

    public function getUrlRu(): ?string
    {
        return $this->urlRu;
    }

    public function setUrlRu(?string $urlRu): self
    {
        $this->urlRu = $urlRu;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNewPrice()
    {
        return $this->newPrice;
    }

    /**
     * @param mixed $newPrice
     */
    public function setNewPrice($newPrice)
    {
        $this->newPrice = $newPrice;
    }

    public function getDiscountPrice()
    {
        $price = 0;
        if ($this->newPrice) {
            $price = $this->newPrice;
        } else {
            $price = round($this->price - ($this->precent * ($this->price / 100)), 2);
        }
        return $price;
    }

    public function getPrice()
    {
        return $this->usd;
    }

    public function getSize()
    {
        $option = [
            [
                'name' => 'XS',
            ],
            [
                'name' => 'S',
            ],
            [
                'name' => 'M',
            ],
            [
                'name' => 'L',
            ],
            [
                'name' => 'XL',
            ]
        ];
        return $option;
    }

    public function __toString(): string
    {
       return $this->getName();
    }
}
