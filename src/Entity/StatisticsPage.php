<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\StatisticsPageRepository")
 */
class StatisticsPage
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ipClient;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $cookiesIdentity;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $page;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIpClient(): ?string
    {
        return $this->ipClient;
    }

    public function setIpClient(string $ipClient): self
    {
        $this->ipClient = $ipClient;

        return $this;
    }

    public function getCookiesIdentity(): ?string
    {
        return $this->cookiesIdentity;
    }

    public function setCookiesIdentity(string $cookiesIdentity): self
    {
        $this->cookiesIdentity = $cookiesIdentity;

        return $this;
    }

    public function getPage(): ?string
    {
        return $this->page;
    }

    public function setPage(string $page): self
    {
        $this->page = $page;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
