<?php

namespace App\Service;


use App\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class Auth
{
    private $passwordEncoder;
    private $entityManager;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder, EntityManagerInterface $entityManager)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->entityManager = $entityManager;
    }

    public function createdUser($request)
    {
        $user = new User();
        $data = [
            'username' => $request->get('username'),
            'password' => $this->passwordEncoder->encodePassword(
                $user,
                $request->get('password')
            ),
            'secretCode' => $request->get('secretCode'),
        ];
        if ($data['username'] && $data['password'] && $data['secretCode']) {
            $user->setUsername($data['username']);
            $user->setPassword($data['password']);
            $user->setSecretCode($data['secretCode']);
            $user->setRoles(['ROLE_USER']);
            $this->entityManager->persist($user);
            $this->entityManager->flush();
        }
    }
}