<?php

namespace App\Service;

use App\Entity\Statistics;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\Storage\SessionStorageInterface;

class StatisticsServices
{
    private $em;
    private $sessionStorage;

    public function __construct(EntityManagerInterface $entityManager, SessionStorageInterface $sessionStorage)
    {
        $this->em = $entityManager;
        $this->sessionStorage = $sessionStorage;
    }

    public function saveServices($productId, $type)
    {
        $infoIp = json_decode($this->getip()->getContent(), true);
        $modelStatistics = new Statistics();
        $modelStatistics->setProductId($productId);
        if (!empty($infoIp['geoplugin_request'])) {
            $modelStatistics->setIpClient($infoIp['geoplugin_request']);
        }
        if (!empty($infoIp['geoplugin_city'])) {
            $modelStatistics->setCity($infoIp['geoplugin_city']);
        }
        if (!empty($infoIp['geoplugin_region'])) {
            $modelStatistics->setRegion($infoIp['geoplugin_region']);
        }
        if (!empty($infoIp['geoplugin_countryName'])) {
            $modelStatistics->setCountry($infoIp['geoplugin_countryName']);
        }
        $modelStatistics->setProductType($type);
        $modelStatistics->setCookiesIdentity($this->sessionStorage->getId());
        $modelStatistics->setCreatedAt(new \DateTime("now"));
        if (!empty($infoIp['geoplugin_countryName'])) {
            $this->em->persist($modelStatistics);
            $this->em->flush();
        }
    }

    private function getip()
    {
        $client = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote = @$_SERVER['REMOTE_ADDR'];
        $result = array('country' => '', 'city' => '');

        if (filter_var($client, FILTER_VALIDATE_IP)) $ip = $client;
        elseif (filter_var($forward, FILTER_VALIDATE_IP)) $ip = $forward;
        else $ip = $remote;
        
        $ip_data = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));

        return new JsonResponse($ip_data);
    }
}