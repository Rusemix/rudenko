<?php

namespace App\Service;

use App\Dto\ContactMessageRequest;
use App\Entity\Contacts;
use App\Entity\Order;
use App\Entity\Preorder;
use Doctrine\ORM\EntityManagerInterface;

class AdminService
{
    private $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    public function contactMessage($request): void
    {
        $contact = new Contacts();
        $contact->setName($request->name);
        $contact->setEmail($request->email);
        $contact->setMessage($request->message);
        $contact->setCreatedAt(date_create(date('Y-m-d H:i:s')));
        $this->em->persist($contact);
        $this->em->flush();
    }

    public function countNewData(): ?array
    {
        $out = [];
        $services = $this->em->getRepository(\App\Entity\Services::class)->findBy(['status' => 1]);
        $order = $this->em->getRepository(Order::class)->findBy(['status' => 1]);
        $preorder = $this->em->getRepository(Preorder::class)->findBy(['status' => 1]);
        if (!empty($services)) {
            $out['service']['sum'] = 1;
            foreach ($services as $value) {
                if (empty($out['service'][$value->getType()])) {
                    $out['service'][$value->getType()] = 1;
                } else {
                    $out['service'][$value->getType()] = $out['service'][$value->getType()] + 1;
                }
                $out['service']['sum'] = $out['service']['sum']+1;
            }
        }
        if (!empty($services)) {
            for ($i = 1; $i <= count($order); $i++) {
                $out['order'] = $i;
            }
        }
        if (!empty($preorder)) {
            for ($i = 1; $i <= count($preorder); $i++) {
                $out['preorder'] = $i;
            }
        }
        return $out;
    }
}