<?php

namespace App\Service;

use App\Dto\AddServicesRequest;
use App\Entity\ClientServices;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

class Services
{
    const TYPE = [
        1 => 'Us Army Consultation',
        2 => 'Work in the USA',
        3 => 'Personal Consultation',
        4 => 'Cooperation',
        5 => 'Advertising in YOUTUBE',
        6 => 'Immigration in the USA'
    ];
    const PAY = [
        1 => true,
        2 => false,
        3 => true,
        4 => false,
        5 => false,
        6 => true
    ];

    const US_ARMY_CONSULTATION_AMOUNT = 30;
    const PERSONAL_CONSULTATION_AMOUNT = 20;
    const IMMIGRATION_IN_THE_USA_AMOUNT = 20;

    private $entityManager;
    private $paymentServices;

    public function __construct(EntityManagerInterface $entityManager, PaymentServices $paymentServices)
    {
        $this->entityManager = $entityManager;
        $this->paymentServices = $paymentServices;
    }

    public function addServices(AddServicesRequest $request): array
    {
        $amount = 0;
        $createPayment = null;
        $statusPay = false;
        if ($request->getType() == 1) {
            $amount = self::US_ARMY_CONSULTATION_AMOUNT;
        } elseif ($request->getType() == 3) {
            $amount = self::PERSONAL_CONSULTATION_AMOUNT;
        } elseif ($request->getType() == 6) {
            $amount = self::IMMIGRATION_IN_THE_USA_AMOUNT;
        }
        if ($amount != 0) {
            $statusPay = true;
        }

        $requestInfo = Request::createFromGlobals();
        $modelServices = new \App\Entity\Services();
        $convertDate = strtotime(str_replace(['PM', 'AM'], '', $request->getDatetime()));
        $date = date('Y-m-d H:i', $convertDate);
        $time = date('Y-m-d H:i', $convertDate);
        $modelServices->setPayment(false);
        $modelServices->setPaymentSuccess(false);
        $modelServices->setName($request->getName());
        $modelServices->setName($request->getName());
        $modelServices->setEmail($request->getEmail());
        $modelServices->setMessage($request->getMessage());
        $modelServices->setType($request->getType());
        $modelServices->setStatus(true);
        $modelServices->setPayment($statusPay);
        $modelServices->setIpClient($requestInfo->server->get('REMOTE_ADDR'));
        $modelServices->setDate(\DateTime::createFromFormat('Y-m-d H:i', $date));
        $modelServices->setTime(\DateTime::createFromFormat('Y-m-d H:i', $time));
        $this->entityManager->persist($modelServices);
        $this->entityManager->flush();

        return [
            'id' => $modelServices->getId(),
            'name' => $modelServices->getName(),
            'type' => self::TYPE[$modelServices->getType()],
            'email' => $modelServices->getEmail(),
            'message' => $modelServices->getMessage(),
            'datetime' => $modelServices->getDate(),
            'amount' => $amount,
            'pay' => $statusPay,
        ];
    }

}