<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PaymentServices
{
    private $em;

    public function __construct(ContainerInterface $container, EntityManagerInterface $entityManager)
    {
        $stripeSecretKey = $container->getParameter('stripe_secret_key');
        \Stripe\Stripe::setApiKey($stripeSecretKey);
        $this->em = $entityManager;
    }

    function calculateOrderAmount($amount): int
    {
        return $amount * 100;
    }

    public function createPayment($data)
    {
        $customer = \Stripe\Customer::create([
            'email' => $data->email,
            'name' => $data->name,
            'description' => 'Type:' . $data->type . ' Date: ' . $data->datetime,
        ]);
        $paymentIntent = \Stripe\PaymentIntent::create([
            'amount' => $this->calculateOrderAmount($data->amount),
            'currency' => 'usd',
            'customer' => $customer->id
        ]);
        $services = $this->em->getRepository(\App\Entity\Services::class)->find($data->serviceId);
        $services->setPaymentCode($paymentIntent->id);
        $this->em->persist($services);
        $this->em->flush();
        return $paymentIntent->client_secret;
    }
}