<?php

namespace App\Service;

use App\Dto\MerchInfoRequest;
use App\Dto\ProductInfoRequest;
use App\Dto\SaveOrderRequest;
use App\Dto\SavePreorderRequest;
use App\Entity\Attachment;
use App\Entity\AttachmentMerch;
use App\Entity\Merch;
use App\Entity\Order;
use App\Entity\Preorder;
use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;

class ProductServices
{
    private $em;
    private $statisticsServices;

    public function __construct(EntityManagerInterface $entityManager, StatisticsServices $statisticsServices)
    {
        $this->em = $entityManager;
        $this->statisticsServices = $statisticsServices;
    }

    public function merchInfo(MerchInfoRequest $request): array
    {
        $out = [];
        if (isset($request->productId)) {
            $productId = $request->productId;
            $product = $this->em->getRepository(Merch::class)
                ->findOneBy(['id' => $productId]);
            $attacment = $this->em->getRepository(AttachmentMerch::class)
                ->findBy(['product' => $productId]);
            $liqpay = new LiqPay('sandbox_i43087449819', 'sandbox_hFAXzhA1fpBKE3d1VPNi7otakVpNChRBS6s2VE9q');
            $title = $product->getName($request->locale);
            $out = [
                'productId' => $product->getId(),
                'name' => $title,
                'description' => $product->getDescription($request->locale),
                'image' => $product->getImage(),
                'url' => $product->getUrl(),
                'urlUa' => $product->getUrlUa(),
                'urlRu' => $product->getUrlRu(),
                'priceDiscount' => $product->getDiscountPrice(),
                'price' => $product->getPrice(),
                'uah' => $product->getUah(),
                'rub' => $product->getRub(),
                'promocode' => $product->getPromocode(),
                'size' => ($product->getCategory()[0]->getId() == 29 || $product->getCategory()[0]->getId() == 11) ? 1 : 0,
                'pay' => []
            ];
            foreach ($attacment as $image) {
                $out['attacment'][] = $image->getImage();
            }
            $this->statisticsServices->saveServices($product->getId(), 1);
        }
        return $out;
    }

    public function productInfo(ProductInfoRequest $request): array
    {
        $out = [];
        if (isset($request->productId)) {
            $productId = $request->productId;
            $product = $this->em->getRepository(Product::class)
                ->findOneBy(['id' => $productId]);
            $attacment = $this->em->getRepository(Attachment::class)
                ->findBy(['product' => $productId]);
            $out = [
                'name' => $product->getTitle(),
                'image' => $product->getImage(),
                'url' => $product->getUrl(),
                'urlAmazon' => $product->getBuyAmazon(),
                'description' => $product->getDescription(),
                'precent' => $product->getNewPrecent(),
                'price' => $product->getPrice(),
                'priceDiscount' => $product->getDiscountPrice(),
                'uah' => $product->getUah(),
                'rub' => $product->getRub(),
                'promocode' => $product->getPromocode(),
            ];
            foreach ($attacment as $image) {
                $out['attacment'][] = $image->getImage();
            }
            $this->statisticsServices->saveServices($productId, 2);
        }
        return $out;
    }

    public function saveOrder(SaveOrderRequest $data): int
    {
        parse_str($data->data, $output);
        $orderModel = new Order();
        $orderModel->setUser($output['surname_name']);
        $orderModel->setEmail($output['email']);
        $orderModel->setPhone($output['phone']);
        $orderModel->setCountry($data->country);
        $orderModel->setCity($output['city'] . ' ' . $output['address']);
        if (!empty($output['typePay'])) {
            $orderModel->setTypePay($output['typePay']);
        }
        if (!empty($output['countProduct'])) {
            $orderModel->setCount($output['countProduct']);
        }
        if (!empty($output['countryOrder'])) {
            $orderModel->setCount($output['countryOrder']);
        }
        if (!empty($output['priceOrder'])) {
            $orderModel->setCount($output['priceOrder']);
        }
        if (!empty($output['nova_poshta'])) {
            $orderModel->setNovaPoshta($output['nova_poshta']);
        }
        if (!empty($output['size'])) {
            $services = 'Size: ' . $output['size'];
            $orderModel->setService($services);
        }
        $orderModel->setStatus(1);
        $product = $this->em->getRepository(Merch::class)
            ->findOneBy(['id' => $output['productId']]);
        if (!empty($product)) {
            $orderModel->setProduct($product);
        }
        $this->em->persist($orderModel);
        $this->em->flush();

        return $orderModel->getId();
    }

    public function savePreorder(SavePreorderRequest $data): int
    {
        parse_str($data->data, $output);
        $orderModel = new Preorder();
        $orderModel->setUser($output['surname_name']);
        $orderModel->setEmail($output['email']);
        $orderModel->setPhone($output['phone']);
        $orderModel->setCity($output['city']);
        $orderModel->setAddress($output['address']);
        $orderModel->setCountry($output['country']);
        if (!empty($output['size'])) {
            $services = 'Size: ' . $output['size'];
            $orderModel->setSizeOption($services);
        }
        if (!empty($output['socialNetwork'])) {
            $orderModel->setSocialNetwork($output['socialNetwork']);
        }
        $orderModel->setStatus(1);
        $product = $this->em->getRepository(Merch::class)
            ->findOneBy(['id' => $output['productId']]);
        if (!empty($product)) {
            $orderModel->setProduct($product);
        }
        $this->em->persist($orderModel);
        $this->em->flush();
        return $orderModel->getId();
    }
}