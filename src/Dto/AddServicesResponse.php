<?php

namespace App\Dto;

use Symfony\Component\Validator\Constraints as Assert;

class AddServicesResponse
{
    /**
     * @Assert\Type("integer")
     * @Assert\NotBlank()
     * @Assert\Positive()
     */
    private $id;

    /**
     * @Assert\Type("string")
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @Assert\Type("string")
     * @Assert\NotBlank()
     */
    private $type;

    /**
     * @Assert\Email()
     * @Assert\NotBlank()
     */
    private $email;

    /**
     * @Assert\Type("string")
     * @Assert\NotBlank()
     */
    private $message;

    /**
     * @Assert\Type("bool")
     */
    private $pay;

    /**
     * @Assert\Type("integer")
     */
    private $amount;

    /**
     * @Assert\Type("string")
     * @Assert\NotBlank()
     */
    private $datetime;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function setMessage($message)
    {
        $this->message = $message;
    }

    public function getDatetime()
    {
        return $this->datetime;
    }

    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;
    }

    public function getPay()
    {
        return $this->pay;
    }

    public function setPay($pay): void
    {
        $this->pay = $pay;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function setAmount($amount): void
    {
        $this->amount = $amount;
    }
}