<?php

namespace App\Dto;

use Symfony\Component\Validator\Constraints as Assert;

class MerchInfoRequest
{
    /**
     * @Assert\NotBlank()
     * @Assert\Type("integer")
     * @Assert\Positive()
    */
    public $productId;

    /**
     * @Assert\Type("string")
     * @Assert\Length(2)
    */
    public $locale;
}