<?php

namespace App\Dto;

use Symfony\Component\Validator\Constraints as Assert;

class CreatePaymentRequest
{
    /**
     * @Assert\Type("integer")
    */
    public $amount;

    public $serviceId;

    /**
     * @Assert\Type("string")
     */
    public $datetime;

    /**
     * @Assert\Type("string")
     */
    public $email;

    /**
     * @Assert\Type("string")
     */
    public $name;

    /**
     * @Assert\Type("string")
     */
    public $type;
}