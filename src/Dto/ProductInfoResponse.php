<?php

namespace App\Dto;

use Symfony\Component\Validator\Constraints as Assert;

class ProductInfoResponse
{
    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
    */
    public $name;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     */
    public $image;

    /**
     * @Assert\Type("string")
     */
    public $url;

    /**
     * @Assert\Type("string")
     */
    public $urlAmazon;

    /**
     * @Assert\Type("string")
     */
    public $description;

    /**
     * @Assert\PositiveOrZero()
     */
    public $precent;

    /**
     * @Assert\PositiveOrZero()
     */
    public $price;

    /**
     * @Assert\PositiveOrZero()
     */
    public $priceDiscount;

    /**
     * @Assert\PositiveOrZero()
     */
    public $uah;

    /**
     * @Assert\PositiveOrZero()
     */
    public $rub;

    /**
     * @Assert\Type("string")
     */
    public $promocode;

    /**
     * @Assert\Type("array")
     */
    public $attacment;
}