<?php

namespace App\Dto;

use Symfony\Component\Validator\Constraints as Assert;

class CreatePaymentResponse
{
    /**
     * @Assert\Type("string")
    */
    public $clientSecret;
}