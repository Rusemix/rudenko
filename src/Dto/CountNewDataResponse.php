<?php

namespace App\Dto;

use Symfony\Component\Validator\Constraints as Assert;

class CountNewDataResponse
{
    /**
     * @Assert\Type("array")
    */
    public $countNewData;
}