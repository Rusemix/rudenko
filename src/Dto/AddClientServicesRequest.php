<?php

namespace App\Dto;

use Symfony\Component\Validator\Constraints as Assert;

class AddClientServicesRequest
{
    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     */
    private $email;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     */
    private $name;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     */
    private $code;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("bool")
     */
    private $status;

    /**
     * @Assert\Type("float")
     */
    private $cost;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     */
    private $authCode;

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code)
    {
        $this->code = $code;
    }

    public function getStatus(): bool
    {
        return $this->status;
    }

    public function setStatus(bool $status)
    {
        $this->status = $status;
    }

    public function getCost(): float
    {
        return $this->cost;
    }

    public function setCost(float $cost)
    {
        $this->cost = $cost;
    }

    public function getAuthCode(): string
    {
        return $this->authCode;
    }

    public function setAuthCode(string $authCode)
    {
        $this->authCode = $authCode;
    }
}