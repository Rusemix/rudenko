<?php

namespace App\Dto;

use Symfony\Component\Validator\Constraints as Assert;

class AddServicesRequest
{
    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     */
    private $name;

    /**
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("integer")
     */
    private $type;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     */
    private $message;

    /**
     * @Assert\Type("string")
     */
    private $codeAuth;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     */
    private $datetime;

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function setMessage($message)
    {
        $this->message = $message;
    }

    public function getDatetime()
    {
        return $this->datetime;
    }

    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;
    }

    public function getCodeAuth()
    {
        return $this->codeAuth;
    }

    public function setCodeAuth($codeAuth)
    {
        $this->codeAuth = $codeAuth;
    }
}