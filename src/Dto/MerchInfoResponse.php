<?php

namespace App\Dto;

use Symfony\Component\Validator\Constraints as Assert;

class MerchInfoResponse
{
    /**
     * @Assert\Type("integer")
     * @Assert\Positive()
     * @Assert\NotBlank()
    */
    public $productId;

    /**
     * @Assert\Type("string")
     * @Assert\NotBlank()
     */
    public $name;

    /**
     * @Assert\Type("string")
     */
    public $description;

    /**
     * @Assert\Type("string")
     */
    public $image;

    /**
     * @Assert\Type("string")
     */
    public $url;

    /**
     * @Assert\Type("string")
     */
    public $urlUa;

    /**
     * @Assert\Type("string")
     */
    public $urlRu;

    /**
     * @Assert\PositiveOrZero()
     */
    public $priceDiscount;

    /**
     * @Assert\PositiveOrZero()
     */
    public $price;

    /**
     * @Assert\PositiveOrZero()
     */
    public $uah;

    /**
     * @Assert\PositiveOrZero()
     */
    public $rub;

    /**
     * @Assert\Type("string")
     */
    public $promocode;

    /**
     * @Assert\Type("integer")
     */
    public $size;

    /**
     * @Assert\Type("array")
     */
    public $pay;

    /**
     * @Assert\Type("array")
     */
    public $attacment;
}