<?php

namespace App\Dto;

use Symfony\Component\Validator\Constraints as Assert;

class ContactMessageRequest
{
    /**
     * @Assert\Type("string")
     * @Assert\NotBlank()
    */
    public $name;

    /**
     * @Assert\Email()
     * @Assert\NotBlank()
    */
    public $email;

    /**
     * @Assert\Type("string")
     * @Assert\NotBlank()
     */
    public $message;
}