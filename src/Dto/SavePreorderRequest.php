<?php

namespace App\Dto;

use Symfony\Component\Validator\Constraints as Assert;

class SavePreorderRequest
{
    /**
     * @Assert\Type("string")
     */
    public $data;
}