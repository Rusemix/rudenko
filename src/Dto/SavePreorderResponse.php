<?php

namespace App\Dto;

use Symfony\Component\Validator\Constraints as Assert;

class SavePreorderResponse
{
    /**
     * @Assert\Type("integer")
     */
    public $orderId;

    /**
     * @Assert\Type("bool")
     */
    public $status;
}