<?php

namespace App\Dto;

use Symfony\Component\Validator\Constraints as Assert;

class ProductInfoRequest
{
    /**
     * @Assert\NotBlank()
     * @Assert\Type("integer")
     * @Assert\Positive()
     */
    public $productId;
}