<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Merch;
use App\Entity\Product;
use App\Entity\TermsOfUse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use App\Service\LiqPay;

class ProductController extends AbstractController
{
    private $translator;
    private $locale;

    function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
        $this->locale = $translator->getLocale();
    }

    public function clothes()
    {
        $page = 0;
        $category = 0;
        if (!empty($_REQUEST['page'])) {
            $page = $_REQUEST['page'];
        }
        if (!empty($_REQUEST['category'])) {
            $category = $_REQUEST['category'];
        }
        $listCategories = $this->getDoctrine()->getRepository(Category::class)->listCategory(2);
        $categoryIds = array_column($listCategories, 'id');
        $items = $this->getDoctrine()->getRepository(Product::class)->listProduct($page, $category, $categoryIds);
        $pagination = $this->getDoctrine()->getRepository(Product::class)->pagination($page, $category, $categoryIds);
        $ulrFileProduct = $this->getParameter('uploads_product');
        $title = $this->translator->trans('Clothes');
        $termOf = $this->getDoctrine()->getRepository(TermsOfUse::class)->getText(1, $this->locale);
        return $this->render('product/index.html.twig', [
            'leftMenu' => $listCategories,
            'title' => $title,
            'items' => $items,
            'ulrFileProduct' => $ulrFileProduct,
            'pagination' => $pagination,
            'termOf' => $termOf,
        ]);
    }

    public function footwear()
    {
        $page = 0;
        $category = 0;
        if (!empty($_REQUEST['page'])) {
            $page = $_REQUEST['page'];
        }
        if (!empty($_REQUEST['category'])) {
            $category = $_REQUEST['category'];
        }
        $listCategories = $this->getDoctrine()->getRepository(Category::class)->listCategory(3);
        $categoryIds = array_column($listCategories, 'id');
        $items = $this->getDoctrine()->getRepository(Product::class)->listProduct($page, $category, $categoryIds);
        $pagination = $this->getDoctrine()->getRepository(Product::class)->pagination($page, $category, $categoryIds);
        $ulrFileProduct = $this->getParameter('uploads_product');
        $title = $this->translator->trans('Footwear');
        $termOf = $this->getDoctrine()->getRepository(TermsOfUse::class)->getText(1, $this->locale);
        return $this->render('product/index.html.twig', [
            'leftMenu' => $listCategories,
            'title' => $title,
            'items' => $items,
            'ulrFileProduct' => $ulrFileProduct,
            'pagination' => $pagination,
            'termOf' => $termOf,
        ]);
    }

    public function backpacks()
    {
        $page = 0;
        $category = 0;
        if (!empty($_REQUEST['page'])) {
            $page = $_REQUEST['page'];
        }
        if (!empty($_REQUEST['category'])) {
            $category = $_REQUEST['category'];
        }
        $listCategories = $this->getDoctrine()->getRepository(Category::class)->listCategory(4);
        $categoryIds = array_column($listCategories, 'id');
        $items = $this->getDoctrine()->getRepository(Product::class)->listProduct($page, $category, $categoryIds);
        $pagination = $this->getDoctrine()->getRepository(Product::class)->pagination($page, $category, $categoryIds);
        $ulrFileProduct = $this->getParameter('uploads_product');
        $title = $this->translator->trans('Backpacks');
        $termOf = $this->getDoctrine()->getRepository(TermsOfUse::class)->getText(1, $this->locale);
        return $this->render('product/index.html.twig', [
            'leftMenu' => $listCategories,
            'title' => $title,
            'items' => $items,
            'ulrFileProduct' => $ulrFileProduct,
            'pagination' => $pagination,
            'termOf' => $termOf,
        ]);
    }

    public function knives()
    {
        $page = 0;
        $category = 0;
        if (!empty($_REQUEST['page'])) {
            $page = $_REQUEST['page'];
        }
        if (!empty($_REQUEST['category'])) {
            $category = $_REQUEST['category'];
        }
        $listCategories = $this->getDoctrine()->getRepository(Category::class)->listCategory(5);
        $categoryIds = array_column($listCategories, 'id');
        $items = $this->getDoctrine()->getRepository(Product::class)->listProduct($page, $category, $categoryIds);
        $pagination = $this->getDoctrine()->getRepository(Product::class)->pagination($page, $category, $categoryIds);
        $ulrFileProduct = $this->getParameter('uploads_product');
        $title = $this->translator->trans('Knives');
        $termOf = $this->getDoctrine()->getRepository(TermsOfUse::class)->getText(1, $this->locale);
        return $this->render('product/index.html.twig', [
            'leftMenu' => $listCategories,
            'title' => $title,
            'items' => $items,
            'ulrFileProduct' => $ulrFileProduct,
            'pagination' => $pagination,
            'termOf' => $termOf,
        ]);
    }

    public function equipment()
    {
        $page = 0;
        $category = 0;
        if (!empty($_REQUEST['page'])) {
            $page = $_REQUEST['page'];
        }
        if (!empty($_REQUEST['category'])) {
            $category = $_REQUEST['category'];
        }
        $listCategories = $this->getDoctrine()->getRepository(Category::class)->listCategory(6);
        $categoryIds = array_column($listCategories, 'id');
        $items = $this->getDoctrine()->getRepository(Product::class)->listProduct($page, $category, $categoryIds);
        $pagination = $this->getDoctrine()->getRepository(Product::class)->pagination($page, $category, $categoryIds);
        $ulrFileProduct = $this->getParameter('uploads_product');
        $title = $this->translator->trans('Equipment');
        $termOf = $this->getDoctrine()->getRepository(TermsOfUse::class)->getText(1, $this->locale);
        return $this->render('product/index.html.twig', [
            'leftMenu' => $listCategories,
            'title' => $title,
            'items' => $items,
            'ulrFileProduct' => $ulrFileProduct,
            'pagination' => $pagination,
            'termOf' => $termOf,
        ]);
    }

    public function merch()
    {
        $title = $this->translator->trans('Merch');
        $ulrFileProduct = $this->getParameter('uploads_merch');
        $page = 0;
        $category = 0;
        if (!empty($_REQUEST['page'])) {
            $page = $_REQUEST['page'];
        }
        if (!empty($_REQUEST['category'])) {
            $category = $_REQUEST['category'];
        }
        $listCategories = $this->getDoctrine()->getRepository(Category::class)->listCategory(1);
        $items = $this->getDoctrine()->getRepository(Merch::class)->listProduct($page, $category);

        $pagination = $this->getDoctrine()->getRepository(Merch::class)->pagination($page, $category);
        return $this->render('product/merch.html.twig', [
            'leftMenu' => $listCategories,
            'title' => $title,
            'items' => $items,
            'ulrFileProduct' => $ulrFileProduct,
            'pagination' => $pagination,
        ]);
    }

    public function liqPay()
    {
        $liqpay = new LiqPay('sandbox_i42336255991', 'sandbox_VI4PIMcnWrEOHJNdXUavp4GwMB35wpjRwoAEVClH');
        $pay = $liqpay->cnb_form_raw(array(
            'action' => 'pay',
            'amount' => '25',
            'currency' => 'USD',
            'description' => 'description text',
            'order_id' => 'order_id_1',
            'version' => '3'
        ));
        return $this->render('pay/liqpay.html.twig', [
            'pay' => $pay,
        ]);

    }
}
