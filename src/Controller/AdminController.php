<?php
/**
 * Created by PhpStorm.
 * User: rusem
 * Date: 15.03.2020
 * Time: 17:19
 */

namespace App\Controller;

use App\Entity\Attachment;
use App\Entity\AttachmentMerch;
use App\Entity\Category;
use App\Entity\Merch;
use App\Entity\Product;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use EasyCorp\Bundle\EasyAdminBundle\Event\EasyAdminEvents;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Handler\UploadHandler;

class AdminController extends EasyAdminController
{

    private $uploadHandler;

    public function __construct(UploadHandler $uploadHandler)
    {
        $this->uploadHandler = $uploadHandler;
    }

    public function copyExternalFile($url, $entity, $dir, $productName)
    {
        $newfile = $dir . '/' . $productName;

        copy($url, $newfile);
        $mimeType = mime_content_type($newfile);
        $size = filesize($newfile);
        $finalName = md5(uniqid(rand(), true)) . ".jpg";

        new UploadedFile($newfile, $finalName, $mimeType, $size);

    }

    public function deleteAction()
    {
        $nameEntity = strtolower($this->entity['name']);
        if ($this->request->get('id') != null) {
            switch ($nameEntity) {
                case 'merch':
                    $requestId = $this->request->get('id');
                    $this->deleteMerch($requestId);
                    break;
                case 'product':
                    $requestId = $this->request->get('id');
                    $this->deleteProduct($requestId);
                    break;
                default:
                    return parent::deleteAction();
            }
        } else {
            return parent::deleteAction();
        }
        return $this->redirectToReferrer();
    }

    public function newAction()
    {
        $fields = $this->entity['new']['fields'];
        $entity = $this->executeDynamicMethod('createNew<EntityName>Entity');
        $newForm = $this->executeDynamicMethod('create<EntityName>NewForm', [$entity, $fields]);

        $nameEntity = strtolower($this->entity['name']);
        if ($this->request->get($nameEntity) != null) {
            switch ($nameEntity) {
                case 'merch':
                    $this->newActionMerch($this);
                    break;
                case 'product':
                    $this->newActionProduct($this);
                    break;
                default:
                    return parent::newAction();
            }
        } else {
            return parent::newAction();
        }
        return $this->redirectToReferrer();
    }

    public function editAction()
    {
        $nameEntity = strtolower($this->entity['name']);
        if ($this->request->get($nameEntity) != null) {
            switch ($nameEntity) {
                case 'merch':
                    $this->editActionMerch($this);
                    break;
                case 'product':
                    $this->editActionProduct($this);
                    break;
                default:
                    return parent::editAction();
            }
        } else {
            return parent::editAction();
        }
        return $this->redirectToReferrer();
    }

    private function newActionProduct($data)
    {
        $request = $data->request->get('product');
        $model = new Product();

        if (!empty($request['title'])) {
            $model->setTitle($request['title']);
        }
        if (!empty($request['titleUa'])) {
            $model->setTitleRu($request['titleUa']);
        }
        if (!empty($request['titleRu'])) {
            $model->setTitleUa($request['titleRu']);
        }

        if (!empty($request['description'])) {
            $model->setDescription($request['description']);
        }
        if (!empty($request['descriptionUa'])) {
            $model->setDescriptionUa($request['descriptionUa']);
        }
        if (!empty($request['descriptionRu'])) {
            $model->setDescriptionRu($request['descriptionRu']);
        }

        if (!empty($request['status'])) {
            $model->setStatus($request['status']);
        }

        if (!empty($request['url'])) {
            $model->setUrl($request['url']);
        }

        if (!empty($request['buyAmazon'])) {
            $model->setBuyAmazon($request['buyAmazon']);
        }

        if (!empty($request['price'])) {
            $model->setPrice($request['price']);
        }

        if (!empty($request['newPrice'])) {
            $model->setNewPrice($request['newPrice']);
        }

        if (!empty($request['precent'])) {
            $model->setPrecent($request['precent']);
        }

        if (!empty($request['rub'])) {
            $model->setRub($request['rub']);
        }
        if (!empty($request['uah'])) {
            $model->setUah($request['uah']);
        }

        if (!empty($request['promocode'])) {
            $model->setPromocode($request['promocode']);
        }
        
        if (!empty($request['mainView'])) {
            $model->setMainView($request['mainView']);
        }

        if (!empty($request['category'])) {
            foreach ($request['category'] as $category) {
                $categoryObject = $this->em->getRepository(Category::class)->findOneBy(['id' => $category]);
                $model->addCategory($categoryObject);
            }
        }

        $this->em->persist($model);
        $this->em->flush();

        $idMerch = $model->getId();

        $nameDir = 'product_' . $idMerch;
        $dir = __DIR__ . '/../../public/uploads/product/' . $nameDir;
        if (!file_exists($dir)) {
            mkdir($dir, 0754);
        }
        if (!empty($_FILES['product']['tmp_name']['imageFile']['file'])) {
            $productName = 'original_' . $idMerch . '.jpg';
            $this->copyExternalFile($_FILES['product']['tmp_name']['imageFile']['file'], $this->entity, $dir, $productName);

            $mainImage = $this->em->getRepository(Product::class)->findOneBy(['id' => $idMerch]);
            $mainImage->setImage($nameDir . '/' . $productName);
            $this->em->flush();
        }

        if (!empty($_FILES['product']['tmp_name']['attachment'])) {
            $merchImage = new Attachment();
            foreach ($_FILES['merch']['tmp_name']['attachment'] as $key => $attachment) {
                $productName = 'attachment_' . $key . '_' . $idMerch . '.jpg';
                $this->copyExternalFile($attachment['imageFile']['file'], $this->entity, $dir, $productName);
                $merchImage->setProduct($model);
                $merchImage->setImage($nameDir . '/' . $productName);
                $this->em->persist($merchImage);
                $this->em->flush();
            }
        }

    }
    private function newActionMerch($data)
    {
        $request = $data->request->get('merch');
        $model = new Merch();

        if (!empty($request['title'])) {
            $model->setTitle($request['title']);
        }
        if (!empty($request['titleUa'])) {
            $model->setTitleRu($request['titleUa']);
        }
        if (!empty($request['titleRu'])) {
            $model->setTitleUa($request['titleRu']);
        }

        if (!empty($request['description'])) {
            $model->setDescription($request['description']);
        }
        if (!empty($request['descriptionUa'])) {
            $model->setDescriptionUa($request['descriptionUa']);
        }
        if (!empty($request['descriptionRu'])) {
            $model->setDescriptionRu($request['descriptionRu']);
        }

        if (!empty($request['status'])) {
            $model->setStatus($request['status']);
        }
        if (!empty($request['optionSize'])) {
            $model->setOptionSize($request['optionSize']);
        }

        if (!empty($request['url'])) {
            $model->setUrl($request['url']);
        }
        if (!empty($request['urlRu'])) {
            $model->setUrlRu($request['urlRu']);
        }
        if (!empty($request['urlUa'])) {
            $model->setUrlUa($request['urlUa']);
        }

        if (!empty($request['usd'])) {
            $model->setUsd($request['usd']);
        }

        if (!empty($request['newPrice'])) {
            $model->setNewPrice($request['newPrice']);
        }

        if (!empty($request['precent'])) {
            $model->setPrecent($request['precent']);
        }

        if (!empty($request['rub'])) {
            $model->setRub($request['rub']);
        }
        if (!empty($request['uah'])) {
            $model->setUah($request['uah']);
        }

        if (!empty($request['category'])) {
            foreach ($request['category'] as $category) {
                $categoryObject = $this->em->getRepository(Category::class)->findOneBy(['id' => $category]);
                $model->addCategory($categoryObject);
            }
        }

        $this->em->persist($model);
        $this->em->flush();

        $idMerch = $model->getId();

        $nameDir = 'product_' . $idMerch;
        $dir = __DIR__ . '/../../public/uploads/merch/' . $nameDir;
        if (!file_exists($dir)) {
            mkdir($dir, 0754);
        }
        if (!empty($_FILES['merch']['tmp_name']['imageFile']['file'])) {
            $productName = 'original_' . $idMerch . '.jpg';
            $this->copyExternalFile($_FILES['merch']['tmp_name']['imageFile']['file'], $this->entity, $dir, $productName);

            $mainImage = $this->em->getRepository(Merch::class)->findOneBy(['id' => $idMerch]);
            $mainImage->setImage($nameDir . '/' . $productName);
            $this->em->flush();
        }

        if (!empty($_FILES['merch']['tmp_name']['attachment'])) {
            $merchImage = new AttachmentMerch();
            foreach ($_FILES['merch']['tmp_name']['attachment'] as $key => $attachment) {
                $productName = 'attachment_' . $key . '_' . $idMerch . '.jpg';
                $this->copyExternalFile($attachment['imageFile']['file'], $this->entity, $dir, $productName);
                $merchImage->setProduct($model);
                $merchImage->setImage($nameDir . '/' . $productName);
                $this->em->persist($merchImage);
                $this->em->flush();
            }
        }

    }

    private function deleteMerch($id)
    {
        $dir = __DIR__ . '/../../public/uploads/merch/product_' . $id;
        if (file_exists($dir)) {
            foreach (scandir($dir) as $file) {
                if ('.' === $file || '..' === $file) continue;
                if (is_dir("$dir/$file")) rmdir_recursive("$dir/$file");
                else unlink("$dir/$file");
            }
            rmdir($dir);
        }
//        $merch = $this->em->getRepository(Merch::class)->findOneBy(['id' => $id]);
        $this->em->getRepository(AttachmentMerch::class)->deleteAttachmentMerch($id);
        $merch = $this->em->getRepository(Merch::class)->deleteMerch($id);

//        if ($merch) {
//            $this->em->remove($merch);
//            $this->em->flush();
//        }    dump($merch);
//        exit();
    }

    private function deleteProduct($id)
    {
        $dir = __DIR__ . '/../../public/uploads/product/product_' . $id;
        if (file_exists($dir)) {
            foreach (scandir($dir) as $file) {
                if ('.' === $file || '..' === $file) continue;
                if (is_dir("$dir/$file")) rmdir_recursive("$dir/$file");
                else unlink("$dir/$file");
            }
            rmdir($dir);
        }
        $product = $this->em->getRepository(Product::class)->findOneBy(['id' => $id]);
        $this->em->getRepository(Attachment::class)->deleteAttachment($id);
        if ($product) {
            $this->em->remove($product);
            $this->em->flush();
        }
    }

    private function editActionMerch($data)
    {
        $request = $data->request->get('merch');
        $id = $data->request->get('id');
        $model = $this->em->getRepository(Merch::class)->findOneBy(['id' => $id]);

        if (!empty($request['imageFile']['delete']) && $request['imageFile']['delete'] == true) {
            $model->setImage('');
            $file = __DIR__ . '/../../public/uploads/merch/product_' . $id . '/original_' . $id . '.jpg';
            if (file_exists($file)) {
                unlink($file);
            }
        }
        if (!empty($request['title'])) {
            $model->setTitle($request['title']);
        }
        if (!empty($request['titleUa'])) {
            $model->setTitleRu($request['titleUa']);
        }
        if (!empty($request['titleRu'])) {
            $model->setTitleUa($request['titleRu']);
        }

        if (!empty($request['description'])) {
            $model->setDescription($request['description']);
        }
        if (!empty($request['descriptionUa'])) {
            $model->setDescriptionUa($request['descriptionUa']);
        }
        if (!empty($request['descriptionRu'])) {
            $model->setDescriptionRu($request['descriptionRu']);
        }

        if (!empty($request['status'])) {
            $model->setStatus($request['status']);
        }
        if (!empty($request['optionSize'])) {
            $model->setOptionSize($request['optionSize']);
        }

        if (!empty($request['url'])) {
            $model->setUrl($request['url']);
        }
        if (!empty($request['urlRu'])) {
            $model->setUrlRu($request['urlRu']);
        }
        if (!empty($request['urlUa'])) {
            $model->setUrlUa($request['urlUa']);
        }

        if (!empty($request['usd'])) {
            $model->setUsd($request['usd']);
        }
        if (!empty($request['newPrice'])) {
            $model->setNewPrice($request['newPrice']);
        }

        if (!empty($request['precent'])) {
            $model->setPrecent($request['precent']);
        }

        if (!empty($request['rub'])) {
            $model->setRub($request['rub']);
        }
        if (!empty($request['uah'])) {
            $model->setUah($request['uah']);
        }

        if (!empty($request['category'])) {
            foreach ($request['category'] as $category) {
                $categoryObject = $this->em->getRepository(Category::class)->findOneBy(['id' => $category]);
                $model->addCategory($categoryObject);
            }
        }

        $this->em->persist($model);
        $this->em->flush();

        $idMerch = $model->getId();

        $nameDir = 'product_' . $idMerch;
        $dir = __DIR__ . '/../../public/uploads/merch/' . $nameDir;
        if (!file_exists($dir)) {
            mkdir($dir, 0754);
        }
        if (!empty($_FILES['merch']['tmp_name']['imageFile']['file']) && !empty($_FILES['merch']['tmp_name']['imageFile']['file'])) {

            $productName = 'original_' . $idMerch . '.jpg';
            $this->copyExternalFile($_FILES['merch']['tmp_name']['imageFile']['file'], $this->entity, $dir, $productName);

            $mainImage = $this->em->getRepository(Merch::class)->findOneBy(['id' => $idMerch]);
            $mainImage->setImage($nameDir . '/' . $productName);
            $this->em->flush();
        }
        if (!empty($_FILES['merch']['tmp_name']['attachment'])) {
            $i = 0;
            foreach ($_FILES['merch']['tmp_name']['attachment'] as $key => $attachment) {
                if ($attachment['imageFile']['file']) {
                    $merchImage = new AttachmentMerch();
                    $productName = 'attachment_' . $key . '_' . $idMerch . '.jpg';
                    $this->copyExternalFile($attachment['imageFile']['file'], $this->entity, $dir, $productName);
                    $merchImage->setProduct($model);
                    $merchImage->setImage($nameDir . '/' . $productName);
                    $this->em->persist($merchImage);
                    $this->em->flush();
                    $i++;
                }
            }
        }
    }

    private function editActionProduct($data)
    {
        $request = $data->request->get('product');
        $id = $data->request->get('id');
        $model = $this->em->getRepository(Product::class)->findOneBy(['id' => $id]);

        if (isset($request['imageFile']['delete']) && $request['imageFile']['delete'] == true) {
            $model->setImage('');
            $file = __DIR__ . '/../../public/uploads/product/product_' . $id . '/original_' . $id . '.jpg';
            if (file_exists($file)) {
                unlink($file);
            }
        }
        if (!empty($request['title'])) {
            $model->setTitle($request['title']);
        }
        if (!empty($request['titleUa'])) {
            $model->setTitleRu($request['titleUa']);
        }
        if (!empty($request['titleRu'])) {
            $model->setTitleUa($request['titleRu']);
        }

        if (!empty($request['description'])) {
            $model->setDescription($request['description']);
        }
        if (!empty($request['descriptionUa'])) {
            $model->setDescriptionUa($request['descriptionUa']);
        }
        if (!empty($request['descriptionRu'])) {
            $model->setDescriptionRu($request['descriptionRu']);
        }

        if (!empty($request['status'])) {
            $model->setStatus($request['status']);
        }
        if (!empty($request['optionSize'])) {
            $model->setOptionSize($request['optionSize']);
        }

        $model->setUrl($request['url']);
        $model->setBuyAmazon($request['buy_amazon']);


        if (!empty($request['urlRu'])) {
            $model->setUrlRu($request['urlRu']);
        }
        if (!empty($request['urlUa'])) {
            $model->setUrlUa($request['urlUa']);
        }

        if (!empty($request['price'])) {
            $model->setPrice($request['price']);
        }
        if (!empty($request['newPrice'])) {
            $model->setNewPrice($request['newPrice']);
        }

        if (!empty($request['precent'])) {
            $model->setPrecent($request['precent']);
        }

        if (!empty($request['promocode'])) {
            $model->setPromocode($request['promocode']);
        }

        if (!empty($request['rub'])) {
            $model->setRub($request['rub']);
        }
        if (isset($request['uah'])) {
            $model->setUah($request['uah']);
        }

        if (!empty($request['main_view'])) {
            $mainView = $request['main_view'];
        }else{
            $mainView = null;
        }
        $model->setMainView($mainView);

        if (!empty($request['category'])) {
            foreach ($request['category'] as $category) {
                $categoryObject = $this->em->getRepository(Category::class)->findOneBy(['id' => $category]);
                $model->addCategory($categoryObject);
            }
        }

        $this->em->persist($model);
        $this->em->flush();

        $idMerch = $model->getId();

        $nameDir = 'product_' . $idMerch;
        $dir = __DIR__ . '/../../public/uploads/product/' . $nameDir;
        if (!file_exists($dir)) {
            mkdir($dir, 0754);
        }
        if (!empty($_FILES['product']['tmp_name']['imageFile']['file']) && !empty($_FILES['product']['tmp_name']['imageFile']['file'])) {

            $productName = 'original_' . $idMerch . '.jpg';
            $this->copyExternalFile($_FILES['product']['tmp_name']['imageFile']['file'], $this->entity, $dir, $productName);

            $mainImage = $this->em->getRepository(Product::class)->findOneBy(['id' => $idMerch]);
            $mainImage->setImage($nameDir . '/' . $productName);
            $this->em->flush();
        }
        if (!empty($_FILES['product']['tmp_name']['attachment'])) {
            $i = 0;
            foreach ($_FILES['product']['tmp_name']['attachment'] as $key => $attachment) {
                if ($attachment['imageFile']['file']) {
                    $merchImage = new Attachment();
                    $productName = 'attachment_' . $key . '_' . $idMerch . '.jpg';
                    $this->copyExternalFile($attachment['imageFile']['file'], $this->entity, $dir, $productName);
                    $merchImage->setProduct($model);
                    $merchImage->setImage($nameDir . '/' . $productName);
                    $this->em->persist($merchImage);
                    $this->em->flush();
                    $i++;
                }
            }
        }
    }
}