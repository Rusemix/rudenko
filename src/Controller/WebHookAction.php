<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Services;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class WebHookAction extends AbstractController
{
    private $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    public function webHook(): JsonResponse
    {
        $response['status'] = false;
        $body = @file_get_contents('php://input');
        if (!empty($body)) {
            $data = json_decode($body, true);
            if ($data['type'] == 'charge.succeeded') {
                $servicesModel = $this->em->getRepository(Services::class)->findOneBy(['paymentCode' => $data['data']['object']['payment_intent']]);
                $servicesModel->setPaymentSuccess(true);
                $this->em->persist($servicesModel);
                $this->em->flush();
                $response['status'] = true;
            }
        }
        return new JsonResponse($response);
    }
}