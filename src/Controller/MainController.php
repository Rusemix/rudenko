<?php

namespace App\Controller;

use App\Entity\Advertising;
use App\Entity\Partners;
use App\Entity\Product;
use App\Entity\Slaider;
use App\Entity\TermsOfUse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class MainController extends AbstractController
{
    private $translator;
    private $locale;

    function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
        $this->locale = $translator->getLocale();
    }
    public function index()
    {
        $ulrFileSlide = $this->getParameter('uploads_slider');
        $ulrFileProduct = $this->getParameter('uploads_product');
        $urlPartner = $this->getParameter('uploads_partners');
        $urlAdvertising = $this->getParameter('uploads_advertising');
        $mainCarousel = $this->getDoctrine()->getRepository(Slaider::class)->findList();
        $productMainView = $this->getDoctrine()->getRepository(Product::class)->mainView();
        $partners = $this->getDoctrine()->getRepository(Partners::class)->list();
        $advertising = $this->getDoctrine()->getRepository(Advertising::class)->list();
        $termOf = $this->getDoctrine()->getRepository(TermsOfUse::class)->getText(1, $this->locale);
        return $this->render('main/index.html.twig', [
            'mainCarousel' => $mainCarousel,
            'ulrFileSlide' => $ulrFileSlide,
            'urlPartner' => $urlPartner,
            'urlAdvertising' => $urlAdvertising,
            'productMainView' => $productMainView,
            'ulrFileProduct' => $ulrFileProduct,
            'partners' => $partners,
            'advertising' => $advertising,
            'termOf' => $termOf,
        ]);
    }

    public function contacts()
    {
        return $this->render('main/contacts.html.twig', [ ]);
    }

    public function services()
    {
        return $this->render('main/services.html.twig', [ ]);
    }
}
