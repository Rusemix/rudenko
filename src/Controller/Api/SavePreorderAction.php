<?php

namespace App\Controller\Api;

use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Dto\SavePreorderRequest;
use App\Dto\SavePreorderResponse;
use App\Service\ProductServices;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class SavePreorderAction
{
    private $validator;
    private $normalizer;
    private $merchServices;

    public function __construct(
        ValidatorInterface $validator,
        ObjectNormalizer $normalizer,
        ProductServices $merchServices
    )
    {
        $this->validator = $validator;
        $this->normalizer = $normalizer;
        $this->merchServices = $merchServices;
    }

    public function __invoke(SavePreorderRequest $data): JsonResponse
    {
        $this->validator->validate($data);
        $orderId = $this->merchServices->savePreorder($data);
        $response = new SavePreorderResponse();
        if (!empty($orderId)) {
            $response->orderId = $orderId;
            $response->status = true;
            $this->validator->validate($response);
        } else {
            $response->status = false;
        }
        return new JsonResponse($this->normalizer->normalize($response, null, ['skip_null_values' => true]));
    }
}