<?php

namespace App\Controller\Api;

use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Dto\CountNewDataRequest;
use App\Dto\CountNewDataResponse;
use App\Service\AdminService;
use App\Service\PaymentServices;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class CountNewDataAction
{
    private $validator;
    private $normalizer;
    private $adminService;

    public function __construct(
        ValidatorInterface $validator,
        ObjectNormalizer $normalizer,
        AdminService $adminService
    )
    {
        $this->validator = $validator;
        $this->normalizer = $normalizer;
        $this->adminService = $adminService;
    }

    public function __invoke(CountNewDataRequest $data): JsonResponse
    {
        $this->validator->validate($data);
        $countNewData = $this->adminService->countNewData();
        $response = new CountNewDataResponse();
        if (!empty($countNewData)) {
            $response->countNewData = $countNewData;
            $this->validator->validate($response);
        }
        return new JsonResponse($this->normalizer->normalize($response, null, ['skip_null_values' => true]));
    }
}