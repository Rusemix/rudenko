<?php


namespace App\Controller\Api;


use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Dto\CreatePaymentRequest;
use App\Dto\CreatePaymentResponse;
use App\Dto\ProductInfoRequest;
use App\Dto\ProductInfoResponse;
use App\Service\PaymentServices;
use App\Service\ProductServices;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class CreatePaymentAction
{
    private $validator;
    private $normalizer;
    private $paymentServices;

    public function __construct(
        ValidatorInterface $validator,
        ObjectNormalizer $normalizer,
        PaymentServices $paymentServices
    )
    {
        $this->validator = $validator;
        $this->normalizer = $normalizer;
        $this->paymentServices = $paymentServices;
    }

    public function __invoke(CreatePaymentRequest $data): JsonResponse
    {
        $this->validator->validate($data);
        $clientSecret = $this->paymentServices->createPayment($data);
        $response = new CreatePaymentResponse();
        if (!empty($clientSecret)) {
            $response->clientSecret = $clientSecret;
            $this->validator->validate($response);
        }
        return new JsonResponse($this->normalizer->normalize($response, null, ['skip_null_values' => true]));
    }
}