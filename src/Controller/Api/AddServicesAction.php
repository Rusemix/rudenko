<?php

namespace App\Controller\Api;

use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Dto\AddServicesRequest;
use App\Dto\AddServicesResponse;
use App\Service\Services;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class AddServicesAction
{
    private $validator;
    private $normalizer;
    private $services;

    public function __construct(ValidatorInterface $validator, ObjectNormalizer $normalizer, Services $services)
    {
        $this->validator = $validator;
        $this->normalizer = $normalizer;
        $this->services = $services;
    }

    public function __invoke(AddServicesRequest $data): JsonResponse
    {
        $this->validator->validate($data);
        $result = $this->services->addServices($data);
        $response = new AddServicesResponse();
        if (!empty($result)) {
            $response->setId($result['id']);
            $response->setName($result['name']);
            $response->setEmail($result['email']);
            $response->setType($result['type']);
            $response->setMessage($result['message']);
            $response->setAmount($result['amount']);
            $response->setPay($result['pay']);
            $response->setDatetime($result['datetime']->format('Y-m-d H:i'));
            $this->validator->validate($response);
        }
        return new JsonResponse($this->normalizer->normalize($response, null, ['skip_null_values' => true]));
    }
}