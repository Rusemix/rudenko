<?php


namespace App\Controller\Api;


use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Dto\MerchInfoRequest;
use App\Dto\MerchInfoResponse;
use App\Service\ProductServices;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class MerchInfoAction
{
    private $validator;
    private $normalizer;
    private $merchServices;

    public function __construct(
        ValidatorInterface $validator,
        ObjectNormalizer $normalizer,
        ProductServices $merchServices
    )
    {
        $this->validator = $validator;
        $this->normalizer = $normalizer;
        $this->merchServices = $merchServices;
    }

    public function __invoke(MerchInfoRequest $data): JsonResponse
    {
        $result = $this->merchServices->merchInfo($data);
        $response = new MerchInfoResponse();
        if (!empty($result)) {
            $response->productId = $result['productId'];
            $response->name = $result['name'];
            $response->description = $result['description'];
            $response->image = $result['image'];
            $response->url = $result['url'];
            $response->urlUa = $result['urlUa'];
            $response->urlRu = $result['urlRu'];
            $response->priceDiscount = $result['priceDiscount'];
            $response->price = $result['price'];
            $response->uah = $result['uah'];
            $response->rub = $result['rub'];
            $response->promocode = $result['promocode'];
            $response->size = $result['size'];
            $response->pay = $result['pay'];
            $response->attacment = $result['attacment'];
            $this->validator->validate($response);
        }
        return new JsonResponse($this->normalizer->normalize($response, null, ['skip_null_values' => true]));
    }
}