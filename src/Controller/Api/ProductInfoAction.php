<?php

namespace App\Controller\Api;

use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Dto\ProductInfoRequest;
use App\Dto\ProductInfoResponse;
use App\Service\ProductServices;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class ProductInfoAction
{
    private $validator;
    private $normalizer;
    private $merchServices;

    public function __construct(
        ValidatorInterface $validator,
        ObjectNormalizer $normalizer,
        ProductServices $merchServices
    )
    {
        $this->validator = $validator;
        $this->normalizer = $normalizer;
        $this->merchServices = $merchServices;
    }

    public function __invoke(ProductInfoRequest $data): JsonResponse
    {
        $result = $this->merchServices->productInfo($data);
        $response = new ProductInfoResponse();
        if (!empty($result)) {
            $response->name = $result['name'];
            $response->description = $result['description'];
            $response->image = $result['image'];
            $response->precent = $result['precent'];
            $response->price = $result['price'];
            $response->priceDiscount = $result['priceDiscount'];
            $response->promocode = $result['promocode'];
            $response->rub = $result['rub'];
            $response->uah = $result['uah'];
            $response->url = $result['url'];
            $response->urlAmazon = $result['urlAmazon'];
            $response->attacment = $result['attacment'];
            $this->validator->validate($response);
        }
        return new JsonResponse($this->normalizer->normalize($response, null, ['skip_null_values' => true]));
    }

}