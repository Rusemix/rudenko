<?php

namespace App\Controller\Api;

use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Dto\ContactMessageRequest;
use App\Service\AdminService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class ContactMessageAction
{
    private $validator;
    private $normalizer;
    private $adminService;

    public function __construct(
        ValidatorInterface $validator,
        ObjectNormalizer $normalizer,
        AdminService $adminService
    )
    {
        $this->validator = $validator;
        $this->normalizer = $normalizer;
        $this->adminService = $adminService;
    }

    public function __invoke(ContactMessageRequest $data): JsonResponse
    {
        $this->validator->validate($data);
        $this->adminService->contactMessage($data);
        $response['status'] = true;
        return new JsonResponse($response);
    }
}