<?php

namespace App\Controller;

use App\Entity\Attachment;
use App\Entity\AttachmentMerch;
use App\Entity\Dropshipping;
use App\Entity\Merch;
use App\Entity\Order;
use App\Entity\Preorder;
use App\Entity\Product;
use App\Entity\Services;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Contracts\Translation\TranslatorInterface;
use App\Service\LiqPay;

class ApiController extends AbstractController
{

    protected $requestStack;
    private $translator;
    private $locale;

    public function __construct(RequestStack $requestStack, TranslatorInterface $translator)
    {
        $this->requestStack = $requestStack;
        $this->translator = $translator;
        $this->locale = $translator->getLocale();
    }

//    /**
//     * @Route("/api", name="api")
//     */
//    public function index()
//    {
//        return $this->render('api/index.html.twig', [
//            'controller_name' => 'ApiController',
//        ]);
//    }


    public function dropshippingAdd()
    {
        if (isset($_POST['productId'])) {
            $productId = $_POST['productId'];
            $client = @$_SERVER['HTTP_CLIENT_IP'];
            $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
            $remote = @$_SERVER['REMOTE_ADDR'];
            $result = array('country' => '', 'city' => '');

            if (filter_var($client, FILTER_VALIDATE_IP)) $ip = $client;
            elseif (filter_var($forward, FILTER_VALIDATE_IP)) $ip = $forward;
            else $ip = $remote;

            $info = file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip);
            $product = $this->getDoctrine()->getRepository(Product::class)->findOneBy(['id' => $productId]);
            if ($product) {
                $dropshipping = new Dropshipping();
                $dropshipping->setProduct($product);
                $dropshipping->setInfo($info);
                $dropshipping->setStatus(true);
                $this->getDoctrine()->getManager()->persist($dropshipping);
                $this->getDoctrine()->getManager()->flush();
            }
        }
        return new JsonResponse($info);
    }


    public function statusPay()
    {
        $private_key = 'sandbox_hFAXzhA1fpBKE3d1VPNi7otakVpNChRBS6s2VE9q';
        $data = $_REQUEST['data'];
        $sign = base64_encode(sha1(
            $private_key .
            $data .
            $private_key
            , 1));
        $model = $this->getDoctrine()->getRepository(Order::class)->find(37);
        $model->setService(json_encode($_REQUEST) . '-----_----' . $sign);
        $this->getDoctrine()->getManager()->persist($model);
        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse();
    }

    public function removeImgAttachment()
    {
        if ($_REQUEST['type'] == 'merch') {
            $file = __DIR__ . '/../../public' . $_REQUEST['imgSrc'];
            if (file_exists($file)) {
                unlink($file);
            }
            $imgUrl = str_replace(['/uploads/merch/'], '', $_REQUEST['imgSrc']);
            $attachment = $this->getDoctrine()->getRepository(AttachmentMerch::class)->findOneBy(['image' => $imgUrl]);
            if (!empty($attachment->getId())) {
                $this->getDoctrine()->getRepository(AttachmentMerch::class)->deleteAttachmentMerchById($attachment->getId());
            }
        }
        return new JsonResponse(['status' => true]);
    }


    public function saveOrder()
    {
        if ($this->requestStack->getCurrentRequest()->get('data')) {
            parse_str($this->requestStack->getCurrentRequest()->get('data'), $output);
            $orderModel = new Order();
            $orderModel->setUser($output['surname_name']);
            $orderModel->setEmail($output['email']);
            $orderModel->setPhone($output['phone']);
            $orderModel->setCity($output['city'] . ' ' . $output['address']);
            if (!empty($output['typePay'])) {
                $orderModel->setTypePay($output['typePay']);
            }
            if (!empty($output['countProduct'])) {
                $orderModel->setCount($output['countProduct']);
            }
            if (!empty($output['countryOrder'])) {
                $orderModel->setCount($output['countryOrder']);
            }
            if (!empty($output['priceOrder'])) {
                $orderModel->setCount($output['priceOrder']);
            }
            if (!empty($output['sizeOrder'])) {
                $services = 'Size: ' . $output['sizeOrder'];
                $orderModel->setService($services);
            }
            $orderModel->setStatus(1);
            $product = $this->getDoctrine()->getRepository(Product::class)
                ->findOneBy(['id' => $output['productId']]);
            if (!empty($product)) {
                $orderModel->addProduct($product);
            }
            $this->getDoctrine()->getManager()->persist($orderModel);
            $this->getDoctrine()->getManager()->flush();

            return new JsonResponse(['status' => true, 'orderId' => $orderModel->getId()]);
        } else {
            return new JsonResponse(['status' => false]);
        }

    }

    public function savePreorder()
    {
        if ($this->requestStack->getCurrentRequest()->get('data')) {
            parse_str($this->requestStack->getCurrentRequest()->get('data'), $output);
            $orderModel = new Preorder();
            $orderModel->setUser($output['surname_name']);
            $orderModel->setEmail($output['email']);
            $orderModel->setPhone($output['phone']);
            $orderModel->setCity($output['city']);
            $orderModel->setAddress($output['address']);
//            $orderModel->setCountry($output['country']);
//            $orderModel->setSizeOption($output['sizeOption']);
//            $orderModel->setSocialNetwork($output['socialNetwork']);
//            $orderModel->setCountProduct($output['сountProduct']);
            $orderModel->setStatus(1);
            $product = $this->getDoctrine()->getRepository(Merch::class)
                ->findOneBy(['id' => $output['productId']]);
            if (!empty($product)) {
                $orderModel->setProduct($product);
            }
            $this->getDoctrine()->getManager()->persist($orderModel);
            $this->getDoctrine()->getManager()->flush();

            return new JsonResponse(['status' => true, 'orderId' => $orderModel->getId()]);
        } else {
            return new JsonResponse(['status' => false]);
        }
    }
}
