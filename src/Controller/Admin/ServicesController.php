<?php

namespace App\Controller\Admin;

use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;

class ServicesController extends EasyAdminController
{
    protected function createListQueryBuilder($entityClass, $sortDirection, $sortField = null, $dqlFilter = null)
    {
        $queryBuilder = parent::createListQueryBuilder($entityClass, $sortDirection, $sortField, $dqlFilter);
        if ($this->request->get('type')) {
            $type = $this->request->get('type');
            $queryBuilder->andWhere('entity.type = :type')
                ->setParameter('type', $type);
        }
        return $queryBuilder;
    }

}