<?php
/**
 * Created by PhpStorm.
 * User: rusem
 * Date: 3/29/2020
 * Time: 8:44 PM
 */

namespace App\Controller;

use App\Entity\Attachment;
use App\Entity\AttachmentMerch;
use App\Entity\Category;
use App\Entity\Merch;
use App\Entity\Product;
use App\Entity\Slaider;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use EasyCorp\Bundle\EasyAdminBundle\Event\EasyAdminEvents;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Handler\UploadHandler;

class AdminSlaiderController extends EasyAdminController
{

    private $uploadHandler;

    public function __construct(UploadHandler $uploadHandler)
    {
        $this->uploadHandler = $uploadHandler;
    }

    public function copyExternalFile($url, $entity, $dir, $productName)
    {
        $newfile = $dir . '/' . $productName;

        copy($url, $newfile);
        $mimeType = mime_content_type($newfile);
        $size = filesize($newfile);
        $finalName = md5(uniqid(rand(), true)) . ".jpg";

        new UploadedFile($newfile, $finalName, $mimeType, $size);

    }

    public function newAction()
    {
        $fields = $this->entity['new']['fields'];
        $entity = $this->executeDynamicMethod('createNew<EntityName>Entity');
        $newForm = $this->executeDynamicMethod('create<EntityName>NewForm', [$entity, $fields]);

        $nameEntity = strtolower($this->entity['name']);
        if ($this->request->get($nameEntity) != null) {
            $request = $this->request->get('slaider');
            $model = new Slaider();
            if (isset($request['url'])) {
                $model->setUrl($request['url']);
            }
            $model->setStatus(true);

            $this->em->persist($model);
            $this->em->flush();

            $idModel = $model->getId();

            $nameDir = 'slider_' . $idModel;
            $dir = __DIR__ . '/../../public/uploads/slider/' . $nameDir;
            if (!file_exists($dir)) {
                mkdir($dir, 0754);
            }
            if (isset($_FILES['slaider']['tmp_name']['imageFile']['file']) && !empty($_FILES['slaider']['tmp_name']['imageFile']['file'])) {
                $productName = 'original_' . $idModel . '.jpg';
                $this->copyExternalFile($_FILES['slaider']['tmp_name']['imageFile']['file'], $this->entity, $dir, $productName);

                $mainImage = $this->em->getRepository(Slaider::class)->findOneBy(['id' => $idModel]);
                $mainImage->setImage($nameDir . '/' . $productName);
                $this->em->flush();
            }

        } else {
            return parent::newAction();
        }
        return $this->redirectToReferrer();
    }

    public function editAction()
    {
        $nameEntity = strtolower($this->entity['name']);
        if ($this->request->get($nameEntity) != null) {
            $request = $this->request->get('slaider');
            $id = $this->request->get('id');
            $model = $this->em->getRepository(Slaider::class)->findOneBy(['id' => $id]);
            if (isset($request['url'])) {
                $model->setUrl($request['url']);
            }
            $model->setStatus(true);

            $this->em->persist($model);
            $this->em->flush();

            $idModel = $model->getId();

            $nameDir = 'slider_' . $idModel;
            $dir = __DIR__ . '/../../public/uploads/slider/' . $nameDir;
            if (!file_exists($dir)) {
                mkdir($dir, 0754);
            }
            if (isset($_FILES['slaider']['tmp_name']['imageFile']['file']) && !empty($_FILES['slaider']['tmp_name']['imageFile']['file'])) {
                $productName = 'original_' . $idModel . '.jpg';
                $this->copyExternalFile($_FILES['slaider']['tmp_name']['imageFile']['file'], $this->entity, $dir, $productName);

                $mainImage = $this->em->getRepository(Slaider::class)->findOneBy(['id' => $idModel]);
                $mainImage->setImage($nameDir . '/' . $productName);
                $this->em->flush();
            }

        } else {
            return parent::editAction();
        }
        return $this->redirectToReferrer();
    }

    public function deleteAction()
    {
        if ($this->request->get('id') != null) {
            $requestId = $this->request->get('id');
            $dir = __DIR__ . '/../../public/uploads/slider/slider_' . $requestId;
            if (file_exists($dir)) {
                foreach (scandir($dir) as $file) {
                    if ('.' === $file || '..' === $file) continue;
                    if (is_dir("$dir/$file")) rmdir_recursive("$dir/$file");
                    else unlink("$dir/$file");
                }
                rmdir($dir);
            }
            $model = $this->em->getRepository(Slaider::class)->findOneBy(['id' => $requestId]);
            if ($model) {
                $this->em->remove($model);
                $this->em->flush();
            }
        } else {
            return parent::deleteAction();
        }
        return $this->redirectToReferrer();
    }
}