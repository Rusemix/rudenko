<?php

namespace App\Controller;

use App\Service\Auth;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('/admin');
        }
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();
        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

//    /**
//     * @Route("/created", name="app_create")
//     */
//    public function created(AuthenticationUtils $authenticationUtils, Auth $auth): Response
//    {
//        if ($authenticationUtils->getRequest()->request) {
//            $auth->createdUser($authenticationUtils->getRequest()->request);
//        }
//        $error = $authenticationUtils->getLastAuthenticationError();
//        $lastUsername = $authenticationUtils->getLastUsername();
//        return $this->render('security/created.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
//    }

    public function logout()
    {
        throw new \Exception('This method can be blank - it will be intercepted by the logout key on your firewall');
    }
}
