<?php

namespace App\Repository;

use App\Entity\Merch;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use App\Service\LiqPay;


/**
 * @method Merch|null find($id, $lockMode = null, $lockVersion = null)
 * @method Merch|null findOneBy(array $criteria, array $orderBy = null)
 * @method Merch[]    findAll()
 * @method Merch[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MerchRepository extends ServiceEntityRepository
{
    const COUNT_PRODUCT = 13;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Merch::class);
    }

    public function listProduct(int $page = null, int $category = null): array
    {
        $out = [];
        if ($page == null) {
            $pageNumber = 0;
        } else {
            $pageNumber = self::COUNT_PRODUCT * ($page - 1);
        }

        $listSql = $this->createQueryBuilder('s')
            ->leftJoin('s.category', 'sc')
            ->andWhere('s.status = :status')
            ->setParameter('status', true)
            ->setMaxResults(self::COUNT_PRODUCT)
            ->setFirstResult($pageNumber)
            ->orderBy('sc.id', 'ASC');
        if ($category != null) {
            $listSql->andWhere('sc.id = :categoryId')->setParameter('categoryId', $category);
        }
        $listProducts = $listSql->getQuery()->getResult();

        foreach ($listProducts as $product) {
            $categories = [];
            foreach ($product->getCategory() as $category) {
                $categories[] = [
                    'merch_id' => $product->getId(),
                    'category_id' => $category->getId()
                ];
            }
            if ($product->getNewPrice()) {
                $out[] = [
                    'id' => $product->getId(),
                    'title' => $product->getTitle(),
                    'url' => $product->getUrl(),
                    'image' => !empty($product->getImage()) ? $product->getImage() : '/image/empty_products.jpg',
                    'price' => $product->getUsd(),
                     'newPrice' => $product->getDiscountPrice(),
                    'precent' => $product->getPrecent(),
                    'count' => 2,
                    'uah' => $product->getUah(),
                    'rub' => $product->getRub(),
                    'promocode' => $product->getPromocode(),
                    'status' => $product->getStatus(),
                    'category' => $categories,
                    'option' => !empty($option) ? json_encode($option) : null
                ];
            }
        }
        return $out;
    }

    public function pagination(int $page = null, int $category = null)
    {
        $out = [];
        if ($page == null) {
            $page = 1;
        }
        $listSql = $this->createQueryBuilder('s')
            ->select('count(s.id)')
            ->leftJoin('s.category', 'sc')
            ->andWhere('s.status = :status')
            ->setParameter('status', true)
            ->orderBy('s.id', 'DESC');
        if ($category != null) {
            $listSql->andWhere('sc.id = :categoryId')->setParameter('categoryId', $category);
        }
        $listProducts = $listSql->getQuery()->getResult();

        $queryString = '';
        if(!empty($_SERVER['QUERY_STRING'])){
            $queryString = '&'.str_replace('&&', '',
                    preg_replace('/page=([0-9]){1,2}/', '', $_SERVER['QUERY_STRING']));
        }
        if ($listProducts[0][1] > self::COUNT_PRODUCT) {
            for ($i = 1; $i <= ceil($listProducts[0][1] / self::COUNT_PRODUCT); $i++) {
                if ($i == $page) {
                    $out[] = ['slected' => 'active', 'pages' => $i, 'url' => '?page='.$i.$queryString];
                } else {
                    $out[] = ['slected' => '', 'pages' => $i, 'url' => '?page='.$i.$queryString];
                }
            }
        }
        return $out;
    }

    public function deleteMerch($id)
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = "DELETE FROM merch_category WHERE merch_id=" . $id;
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $sql = "DELETE FROM merch WHERE id=" . $id;
        $stmt = $conn->prepare($sql);
        $stmt->execute();
    }
}
