<?php

namespace App\Repository;

use App\Entity\Category;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends ServiceEntityRepository
{
    private $translator;

    public function __construct(ManagerRegistry $registry, TranslatorInterface $translator)
    {
        parent::__construct($registry, Category::class);
        $this->translator = $translator;
    }

    public function listCategory($idMenu)
    {
        $out = [];
        $fields = '';
        switch ($this->translator->getLocale()) {
            case 'en':
                $fields = 'c.name as name, c.secondName as secondName, c.id, c.status, c.classCss';
                break;
            case 'ua':
                $fields = 'c.nameUa as name, c.secondNameUa as secondName, c.id, c.status, c.classCss';
                break;
            case 'ru':
                $fields = 'c.nameRu as name, c.secondNameRu as secondName, c.id, c.status, c.classCss';
                break;
            default:
        }
        $categorys = $this->createQueryBuilder('c')
            ->select($fields)
            ->andWhere('c.typeCategory = :typeCategory')
            ->andWhere('c.status = :status')
            ->setParameter('typeCategory', $idMenu)
            ->setParameter('status', true)
            ->orderBy('c.weight', 'DESC')
            ->getQuery()
            ->getResult();
        foreach($categorys as $category){
            if(!empty($category['name'])){
                $out[] = $category;
            }
        }
        return $out;
    }

}
