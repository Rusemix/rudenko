<?php

namespace App\Repository;

use App\Entity\TypeCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method TypeCategory|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeCategory|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeCategory[]    findAll()
 * @method TypeCategory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeCategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypeCategory::class);
    }

    // /**
    //  * @return TypeCategory[] Returns an array of TypeCategory objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TypeCategory
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
