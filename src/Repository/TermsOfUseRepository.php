<?php

namespace App\Repository;

use App\Entity\TermsOfUse;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method TermsOfUse|null find($id, $lockMode = null, $lockVersion = null)
 * @method TermsOfUse|null findOneBy(array $criteria, array $orderBy = null)
 * @method TermsOfUse[]    findAll()
 * @method TermsOfUse[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TermsOfUseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TermsOfUse::class);
    }

    public function getText($id, $local)
    {
        $term = $this->find($id);
        $text = $term->getText();
        if ($local == 'en') {
            $text = $term->getText();
        } elseif ($local == 'ua') {
            $text = $term->getTextUa();
        } elseif ($local == 'ru') {
            $text = $term->getTextRu();
        }
        return $text;
    }
}
