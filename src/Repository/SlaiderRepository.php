<?php

namespace App\Repository;

use App\Entity\Slaider;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Slaider|null find($id, $lockMode = null, $lockVersion = null)
 * @method Slaider|null findOneBy(array $criteria, array $orderBy = null)
 * @method Slaider[]    findAll()
 * @method Slaider[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SlaiderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Slaider::class);
    }

    // /**
    //  * @return Slaider[] Returns an array of Slaider objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Slaider
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findList($countResult = 10)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.status = :status')
            ->setParameter('status', true)
            ->orderBy('s.id', 'DESC')
            ->setMaxResults($countResult)
            ->getQuery()
            ->getResult();
    }
}
