<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use App\Service\LiqPay;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{

    const COUNT_PRODUCT = 13;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function mainView()
    {
        $products = $this->createQueryBuilder('s')
            ->andWhere('s.mainView != :mainView')
            ->setParameter('mainView', 0)
            ->orderBy('s.id', 'DESC')
            ->setMaxResults(4)
            ->getQuery()
            ->getResult();
        foreach ($products as $product) {
            $items[$product->getMainView()] = $product;
        }
        return $items;
    }

    public function listProduct(int $page = null, int $category = null, array $categoryIds = []): array
    {
        $out = [];
        if ($page == null) {
            $pageNumber = 0;
        } else {
            $pageNumber = self::COUNT_PRODUCT * ($page - 1);
        }
        $listSql = $this->createQueryBuilder('s')
            ->leftJoin('s.category', 'sc')
            ->andWhere('s.status = :status')
            ->setParameter('status', true)
            ->setMaxResults(self::COUNT_PRODUCT)
            ->setFirstResult($pageNumber)
            ->orderBy('s.id', 'DESC');
        if ($category != null) {
            $listSql->andWhere('sc.id = :categoryId')->setParameter('categoryId', $category);
        }else{
            $listSql->andWhere('sc.id in (:categoryIds)')->setParameter('categoryIds', $categoryIds);
        }
        $listProducts = $listSql->getQuery()->getResult();

        foreach ($listProducts as $product) {
            $categories = [];
            foreach ($product->getCategory() as $category) {
                $categories[] = [
                    'merch_id' => $product->getId(),
                    'category_id' => $category->getId()
                ];
            }
            if ($product->getPrice()) {
                $out[] = [
                    'id' => $product->getId(),
                    'title' => $product->getTitle(),
                    'url' => $product->getUrl(),
                    'image' => $product->getImage(),
                    'price' => $product->getPrice(),
                    'newPrice' => $product->getDiscountPrice(),
                    'precent' => $product->getNewPrecent(),
                    'count' => $product->getCount(),
                    'uah' => $product->getUah(),
                    'rub' => $product->getRub(),
                    'promocode' => $product->getPromocode(),
                    'status' => $product->getStatus(),
                    'category' => $categories,
                    'option' => !empty($option) ? json_encode($option) : null,
                ];
            }
        }
        return $out;
    }

    public function pagination(int $page = null, int $category = null, array $categoryIds = [])
    {
        $out = [];
        if ($page == null) {
            $page = 1;
        }
        $listSql = $this->createQueryBuilder('s')
            ->select('count(s.id)')
            ->leftJoin('s.category', 'sc')
            ->andWhere('s.status = :status')
            ->setParameter('status', true)
            ->orderBy('s.id', 'DESC');
        if ($category != null) {
            $listSql->andWhere('sc.id = :categoryId')->setParameter('categoryId', $category);
        }else{
            $listSql->andWhere('sc.id in (:categoryIds)')->setParameter('categoryIds', $categoryIds);
        }
        $listProducts = $listSql->getQuery()->getResult();

        $queryString = '';
        if(!empty($_SERVER['QUERY_STRING'])){
            $queryString = '&'.str_replace('&&', '',
                    preg_replace('/page=([0-9]){1,2}/', '', $_SERVER['QUERY_STRING']));
        }
        if ($listProducts[0][1] > self::COUNT_PRODUCT) {
            for ($i = 1; $i <= ceil($listProducts[0][1] / self::COUNT_PRODUCT); $i++) {
                if ($i == $page) {
                    $out[] = ['slected' => 'active', 'pages' => $i, 'url' => '?page='.$i.$queryString];
                } else {
                    $out[] = ['slected' => '', 'pages' => $i, 'url' => '?page='.$i.$queryString];
                }
            }
        }
        return $out;
    }
}
