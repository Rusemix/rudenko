<?php

namespace App\Repository;

use App\Entity\AttachmentMerch;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method AttachmentMerch|null find($id, $lockMode = null, $lockVersion = null)
 * @method AttachmentMerch|null findOneBy(array $criteria, array $orderBy = null)
 * @method AttachmentMerch[]    findAll()
 * @method AttachmentMerch[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AttachmentMerchRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AttachmentMerch::class);
    }

    public function deleteAttachmentMerch($id)
    {
        return $this->createQueryBuilder('am')
            ->delete()
            ->andWhere('am.product = :id')
            ->setParameter('id', $id)
            ->getQuery()->getResult();
    }

    public function deleteAttachmentMerchById($id)
    {
        return $this->createQueryBuilder('am')
            ->delete()
            ->andWhere('am.id = :id')
            ->setParameter('id', $id)
            ->getQuery()->getResult();
    }
}
