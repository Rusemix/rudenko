<?php

namespace App\Repository;

use App\Entity\Dropshipping;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Dropshipping|null find($id, $lockMode = null, $lockVersion = null)
 * @method Dropshipping|null findOneBy(array $criteria, array $orderBy = null)
 * @method Dropshipping[]    findAll()
 * @method Dropshipping[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DropshippingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Dropshipping::class);
    }

    // /**
    //  * @return Dropshipping[] Returns an array of Dropshipping objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Dropshipping
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

}
