<?php

namespace App\Repository;

use App\Entity\Preorder;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Preorder|null find($id, $lockMode = null, $lockVersion = null)
 * @method Preorder|null findOneBy(array $criteria, array $orderBy = null)
 * @method Preorder[]    findAll()
 * @method Preorder[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PreorderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Preorder::class);
    }

    // /**
    //  * @return Preorder[] Returns an array of Preorder objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Preorder
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
