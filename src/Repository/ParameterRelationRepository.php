<?php

namespace App\Repository;

use App\Entity\ParameterRelation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ParameterRelation|null find($id, $lockMode = null, $lockVersion = null)
 * @method ParameterRelation|null findOneBy(array $criteria, array $orderBy = null)
 * @method ParameterRelation[]    findAll()
 * @method ParameterRelation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ParameterRelationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ParameterRelation::class);
    }

    // /**
    //  * @return ParameterRelation[] Returns an array of ParameterRelation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ParameterRelation
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
