<?php

namespace App\Repository;

use App\Entity\StatisticsPage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method StatisticsPage|null find($id, $lockMode = null, $lockVersion = null)
 * @method StatisticsPage|null findOneBy(array $criteria, array $orderBy = null)
 * @method StatisticsPage[]    findAll()
 * @method StatisticsPage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StatisticsPageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StatisticsPage::class);
    }

    // /**
    //  * @return StatisticsPage[] Returns an array of StatisticsPage objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?StatisticsPage
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
